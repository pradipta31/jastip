-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Jul 2020 pada 15.48
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jasa_titip`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id_admin` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id_admin`, `username`, `password`, `nama`, `alamat`, `email`, `no_hp`, `foto`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Diah', '$2y$10$Hrd68rAomlwdHKsRpV0FX.y1IWsrtIxIaZqHyi807VA0lH4nMK58.', 'Diah', 'jalan tukad badung', 'diah123@gmail.com', '081913688449', '1592472447.jpg', 'mahasiswa', 1, '2020-06-18 01:24:23', '2020-07-01 21:26:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jastips`
--

CREATE TABLE `jastips` (
  `id_jastip` bigint(20) UNSIGNED NOT NULL,
  `member_id` bigint(20) UNSIGNED NOT NULL,
  `kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jastips`
--

INSERT INTO `jastips` (`id_jastip`, `member_id`, `kota`, `provinsi`, `tanggal_awal`, `tanggal_akhir`, `foto`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tanggerang', 'Banten', '2020-05-04', '2020-05-12', '1581576301.jpeg', 'nitipnyadiDiah \r\n\r\nopen jastip tanggerang, buat yang mau jastip, cuk yuk chat jangan sampek ketinggalan.\r\nSeee you guyss!!', 0, '2020-02-12 23:45:01', '2020-06-20 20:51:49'),
(2, 2, 'Denpasar', 'Bali', '2020-05-01', '2020-05-09', '1581585510.jpeg', 'ana72jastip akan open jastip daerah Denpasar, nah buat kalian yang mau ikutan jastipnya ana72jastip bisa ikutin POnya di mei mendatang.\r\nSee you soon', 0, '2020-02-13 02:18:30', '2020-06-20 21:00:48'),
(3, 3, 'Badung', 'Bali', '2020-05-09', '2020-05-19', '1581585769.jpeg', 'attention guysss!!\r\nopen jastip Singapore daerah  Paragon pusat barang Branded terlengkap, nah kita juga open jastip untuk skin care kekinian, alat elektronik  terbaru saat ini.', 0, '2020-02-13 02:22:49', '2020-06-20 21:35:43'),
(4, 4, 'Semarapura', 'Bali', '2020-04-05', '2020-04-21', '1581585869.jpeg', 'Lia Penitipann\r\n\r\nHallo, nah kali ini lia penitipan membuka penitipan didaerah klungkung yang terkenal dengan pakaian adat bali, perhiasan, hingga alat persembahyangan yang dijamin harga murah kualitas baguss, Lia Penitipan menerima pembelian kamen, kain kebaya, selendang, udeng, emas dll. \r\nNah buat kalian nihh yang gak mau kehabisan barang-barang premium di Kota Semarapura bisa jastip di Lia ya dijamin pengirimannya cepatt.\r\njadi tunggu apalagi sebelum kehabisan slot yahhh dadaaaa', 0, '2020-02-13 02:24:29', '2020-06-20 21:35:57'),
(5, 5, 'Denpasar', 'Bali', '2020-05-04', '2020-05-20', '1581586084.jpeg', 'Nitip Barang Bisa by Novik Rahayu\r\n\r\nOpen Jastip daerah Sanurr!!\r\nhi, kali ini novik bakalann open jastip daerah sanur nihh, yang udah pada ngiler sama makan-makan disanur bisa ikut jastip daerah Sanur kali ini yahhh\r\nselamattt jastip by novikkkk', 0, '2020-02-13 02:28:04', '2020-06-20 21:36:22'),
(6, 6, 'Tabanan', 'Bali', '2020-04-27', '2020-05-02', '1581586154.jpeg', 'Bisa Nitip Apapun sama Ode Mega\r\n\r\nhappy jastip!! nah kali ini special ode bakal open jastip di Tabanan nah kebetulan Tabanan daerah Baturiti yaahh,, nah jastip kali ini kita open makanan di Warung Made, sayur segar, buah segar dan masih banyak lagi.\r\nnah pas banget nih buat kalian yang ada didaerah Baturiti yang mager keluar bisa manfaatin open jastip Ode Mega yaa dijamin tidak mengecewakan\r\nbye byee!!', 0, '2020-02-13 02:29:14', '2020-06-20 21:36:40'),
(7, 7, 'Tebet, Jakarta Selatan', 'DKI Jakarta', '2020-04-27', '2020-05-01', '1581613659.png', 'Nitip Bareng Loly\r\n\r\nNitip Bareng Loly bakalan open jastip di daerah Tebet, nah buat kalian yang mau makakan di daerah Tebet bisa ikut jastip kali ini yahh!\r\nsee you guyss', 0, '2020-02-13 10:07:39', '2020-06-20 21:36:57'),
(8, 8, 'Bandung', 'Jawa Barat', '2020-05-11', '2020-05-22', '1587269329.jpeg', 'JastipnyaMus !\r\n\r\nJastipnyaMus bulan mei ini akan ngadain open jastip dikota Bandung, nah buat kalian yang mau jastip makanan, barang-barang asli bandung bisa ikut jastip bandung kita kali ini yahhh\r\nsee you on may guyssss!!!', 0, '2020-04-18 20:08:49', '2020-06-20 21:37:13'),
(9, 9, 'Tabanan', 'Bali', '2020-05-18', '2020-05-23', '1587269877.jpeg', 'PenitipanAyuDevii\r\n\r\nHi, guys kebetulan banget nih jastip Ayu Devi ngadain jastip di Daerah asal ayu, nah buat kalian yang sempat buat keluar belanja bisa banget jastip di Ayu Devi, nah jastip kali ini Penitipan Ayu Devi nerima segala pembelian dikota Tabanan. Alat-alat (elektronik, rumah tangga, alat beran dll), Makanan, Minuman, Baju, dll.\r\n Selamat bergabung dipenitipan ayu Devi', 0, '2020-04-18 20:17:57', '2020-06-20 21:37:44'),
(10, 10, 'Karangasem', 'Bali', '2020-05-11', '2020-05-16', '1587270587.jpeg', 'Sri Adnyani Jualan!!\r\n\r\nHappy Online guyss!!\r\nOkay, dijastip kali ini Sri Adnyani Jualan bakan open jastip daerah Rendang, Karangasem.  Di jastip kali ini Sri akan open jastip barang-barang yang ada di Desa Rendang yang bakan siap dikirim ke daerah Denpasar so pasti harga dijamin murah karna Sri Adnyani Jualan mengambil langsung barang-barang ke rumah (first hand), selain harga kualitas pasti Okay! open jastipnya bahan-bahan rumah tangga yahh. info lebih jelas bisa langsung hubungi Personal Contact yah guyss!!', 0, '2020-04-18 20:29:47', '2020-06-20 21:38:02'),
(11, 11, 'Badung', 'Bali', '2020-06-09', '2020-06-19', '1588925129.jpeg', 'kita bakan open jastip di mall Beachw Kuta Bali', 0, '2020-05-08 00:05:29', '2020-06-20 21:38:27'),
(14, 13, 'Tabanan', 'Bali', '2020-05-24', '2020-05-30', '1589010101.jpeg', 'Open Jastip khusus beras organik', 0, '2020-05-08 23:41:41', '2020-06-20 21:38:50'),
(15, 12, 'Denpasar', 'Bali', '2020-06-01', '2020-06-08', '1589427243.jpg', 'Hallo Semuaa!!!\r\nNah sekarang aku bakalan open jastip daerah denpasar yaa, untuk jastip kali ini aku mau open untuk alat elektronik: Iphone, Laptop, Apple watch, Ipad, Android Tablet, AirPoda. Buat yang mau jastip bisa langsung chat via WA yang tersedia ya', 0, '2020-05-13 19:34:03', '2020-06-20 21:39:22'),
(16, 1, 'Malang', 'Jawa Timur', '2020-06-02', '2020-06-12', '1592473726.jpeg', 'yuhu jastip Malang!!\r\n\r\nyang mau jastip makanan khas malang, buah-buahan, makanan, pakaian dan apapun itu bisa banget yukk.\r\n\r\nlangsung aja chat agak tidak ketinggalan.', 0, '2020-06-18 01:48:46', '2020-06-20 20:54:58'),
(17, 1, 'Kapal, Badung', 'Bali', '2020-06-15', '2020-06-20', '1592473965.jpeg', 'Special kali ini aku ngadain jastip di Kapal Badung yang terkenal sama  peratalan rumah, Candi, Pot Bunga, Pot Kolam ikan, Patung, Sanggah, Patung dll. nah buat yang mau nitip selain yang aku jelasin bisa banget tinggal chat aja kontak yang sudah tertera ya guyss,, happy jastipp.', 0, '2020-06-18 01:52:45', '2020-06-20 21:29:23'),
(18, 2, 'Gianyar', 'Bali', '2020-05-21', '2020-05-26', '1592715887.jpeg', 'Jastip, Jastipp Gianyar!!\r\n\r\nKhusus untuk kali ini aku ngadain jastip di Gianyar, yang terkenal dengan seni. nah jastip kali ini aku bakal open untuk accesoris Silver yang terkenal di Sukawati, Gianyar. nah buat kalian yang gak mau ketinggalan jastip ini langsung aja chat aku barang yang kalian mau.', 0, '2020-06-20 21:04:48', '2020-06-20 21:05:06'),
(19, 2, 'Denpasar', 'Bali', '2020-06-01', '2020-06-09', '1592716958.jpeg', 'Special jastip kali ini karna kita bakal pulang kampung ke Negara berhubung lagi ada Rahinan, kita bakal open jastip untuk persiapan persembahyangan, Bolu/ Snack, Buah-buahan. nah buat kalian yang mau jastip bisa banget chat aja langsung\r\n\r\nThank You gais', 0, '2020-06-20 21:22:38', '2020-06-20 21:27:30'),
(20, 2, 'Denpasar', 'Bali', '2020-06-16', '2020-06-21', '1592717326.jpeg', 'Haiii, !\r\naku bakal open jastip Perhiasan emas putih dan emas kuning, nah buat kalian yang gak sempat buat beli bisa banget nitip sama kitaa!\r\n\r\nThank you', 0, '2020-06-20 21:28:46', '2020-06-20 22:46:03'),
(21, 1, 'Karangasem', 'Bali', '2020-06-23', '2020-06-26', '1592717705.jpg', 'Open jastip Daerah Karangasem.\r\n\r\nKhusus kali ini, open order special pakaian Tradisional khas Desa Tenganan Pegringsingan, nah kami open order kain legendaris yaitu kain gringsing clasic. \r\nNah yang gak mau ketinggalan edisi jastip spesial kali ini langsung aja chat kami langsung yaa gaisss!', 0, '2020-06-20 21:35:05', '2020-06-26 23:29:40'),
(22, 3, 'Ubud, Gianyar', 'Bali', '2020-06-03', '2020-06-08', '1592718292.jpeg', 'Hai hai, \r\n\r\njastip kali ini yuhuu banget, karna aku open jastip spesial  makanan terkenal yang ada di Ubud, nah yang gak sempat buat langsung ke ubud bisa banget nitip di kita yah\r\n\r\nSee youu,,', 0, '2020-06-20 21:44:52', '2020-06-20 22:53:24'),
(23, 4, 'Denpasar', 'Bali', '2020-06-09', '2020-06-13', '1592718438.jpeg', 'Open Jastip makanan Denpasar, nah buat yang di Karangasem yang udah kangen sama makanan-makanan Denpasar bisa banget makek jasa kita yahh\r\n\r\nThank you', 0, '2020-06-20 21:47:18', '2020-06-20 23:02:45'),
(24, 5, 'Baturiti, Tabanan', 'Bali', '2020-06-11', '2020-06-14', '1592719062.jpeg', 'Open jastip \r\n\r\nOpen Jastip Buah-buahan dan sayur segar dari baturity nah buat kalian yang mau order bisa banget langsung aja chat kita yaa', 0, '2020-06-20 21:57:42', '2020-06-20 23:03:03'),
(25, 6, 'Denpasar', 'Bali', '2020-06-15', '2020-06-20', '1592721616.jpg', 'Yuhuuuyyy!\r\n\r\nOpen jastip Khusus Parfum Men dan Women bisa banget makek jasa kita, kanra Parfum apa aja bisa kalian pesen, jangan lupa chat kita yaa', 0, '2020-06-20 22:15:44', '2020-06-20 23:03:19'),
(26, 7, 'jakarta pusat', 'DKI Jakarta', '2020-06-09', '2020-06-15', '1592720347.jpg', 'Open Jastip!\r\n\r\nNah kali ini aku ngadain jastip khusus Smartphone, smart watch, laptop, ipad dengan merek yang kalian mau ya. Langsung aja chat kami\r\n\r\nThank You', 0, '2020-06-20 22:19:07', '2020-06-20 23:03:43'),
(27, 8, 'Kuta, Badung', 'Bali', '2020-06-11', '2020-06-16', '1592720514.jpeg', 'Open Jastip daerah kuta\r\n\r\nnah yang mau jastip, pakaian, makanan, minuman, benda untuk dikamar, peralatan rumah bisa bangett.\r\nyuk yuk chat kitaa', 0, '2020-06-20 22:21:54', '2020-06-20 23:04:03'),
(28, 9, 'Denpasar', 'Bali', '2020-06-21', '2020-06-29', '1592720873.jpg', 'Hello, open jastipp\r\n\r\nKalii ini kita mau Open jastip barang di mall bali galerya, nah buat kalian yang  mau jastip langsung aja chat kita, jangn sampek ketinggalanan\r\n\r\nsee you', 0, '2020-06-20 22:27:53', '2020-07-05 16:09:41'),
(29, 10, 'Denpasar', 'Bali', '2020-06-30', '2020-07-06', '1593241312.jpeg', 'Open Jastip!\r\n\r\nKami lagi Open jastip di Pasar Kumbasari, nah buat yang gak punya waktu buat belaja bisa banget makek jasa layanan kami yaa\r\nThank You', 1, '2020-06-20 22:30:58', '2020-07-05 14:24:28'),
(30, 11, 'Tabanan', 'Bali', '2020-06-20', '2020-06-24', '1592721301.jpg', 'Haii guyss\r\n\r\nKamis open jastip di kota Tabanan jastipnya alat-alat mesin yaa, yang mau nitip barang-barang ditabanan bisa banget ya,  yang mau titip dengan harga lebih murah yuk yuk', 0, '2020-06-20 22:35:01', '2020-06-26 23:30:27'),
(31, 12, 'Kelungkung', 'Bali', '2020-07-01', '2020-07-07', '1593241043.jpg', 'Jastip yuk\r\n\r\nkali ini kami open jasip Kain adat, pakaian adat dan masih banyak lagi. nah yang mau order dan gak mau ketinggalan jastip ini bisa banget ya, langsung aja chat kami guyss,,', 1, '2020-06-20 22:38:50', '2020-07-05 14:25:34'),
(32, 13, 'Jogjakarta', 'Jogjakarta', '2020-07-02', '2020-07-08', '1592721930.jpg', 'hi hii\r\n\r\nKali ini kami open jastip daerah Istimewa Jogjakarta, open jastip kain-kain batik kas Jogja, Makanan Pie Patok yang sudah terkenal. nah tunggu apalagi buruan chat aja kita langsung yaaa\r\n\r\nSee youuu', 1, '2020-06-20 22:45:30', '2020-07-05 14:26:40'),
(33, 2, 'Kuta, Badung', 'Bali', '2020-06-29', '2020-07-09', '1592722215.jpg', 'Haii Pelanggan Setia jastipp\r\n\r\nKalian ini aku ngadain Jastip Khusu di Beach Walk Kuta Bali, nah kebetulan BW lagi ngadain big sale, yang gak mau ketinggalan salenya bisa pakek jasa layanan kami yahh,\r\nLangsung aja hubungu kontak yang tertera\r\n\r\nThank Youu', 1, '2020-06-20 22:50:15', '2020-07-05 14:35:26'),
(34, 3, 'Kota Gianyar', 'Bali', '2020-06-21', '2020-06-26', '1592722927.jpeg', 'Open Jastip\r\n\r\nSilver yang terkenal di Gianyar, nah kali ini kalian bisa nitip yaa model yang kalian mau, nah tunggu apa lagi yuk chat kita, no sudah tersedia yaa\r\nluv luv', 0, '2020-06-20 23:02:07', '2020-07-05 14:36:09'),
(35, 4, 'Sukawati, Gianyar', 'Bali', '2020-06-18', '2020-06-25', '1592723322.jpeg', 'Hii,\r\n\r\nKali ini kita bakal open jastip khusu alpaka silver dari sukawati yaa, nah yang mau nyari alpaka kualitas ok langsung dari pengrajinnya dengan harga oke. nitip aja sama pelayanan kita yaa\r\n\r\nThank You', 0, '2020-06-20 23:08:42', '2020-07-05 14:27:25'),
(36, 5, 'Tengku Umar, Denpasar', 'Bali', '2020-06-20', '2020-06-28', '1592723492.jpeg', 'hii,\r\n\r\nOpen Jastip, kali ini kami ngadain jastip di Level 21 Mall, kalian bisa nitip apa aja, Pakaian, Makanan, Make Up, Parfum dll ,yuk yuk jastip sama kitaa. \r\nSee Youu', 0, '2020-06-20 23:11:32', '2020-07-05 14:37:02'),
(37, 6, 'Gajah Mada, Denpasar', 'Bali', '2020-06-22', '2020-06-30', '1593241187.jpg', 'Open Jastipp\r\nKain Kebaya, atau apapun itu. lagi buka slot untuk kain bahan baju, celama, rok dll. yuk jastip sama kitaa', 0, '2020-06-20 23:25:57', '2020-07-05 14:38:06'),
(38, 7, 'Denpasar', 'Bali', '2020-07-03', '2020-07-09', '1592724529.jpeg', 'Open Jastip \r\n\r\nKita Open Jastip bahan-bahan Kue segala bahan kue, alat-alat, bahan. nah yang mau tinggal chat aja kita yaa, mau nitip beli yang mana yuk terima apa aja\r\n luv luv', 1, '2020-06-20 23:28:49', '2020-07-05 14:39:10'),
(39, 8, 'Denpasar', 'Bali', '2020-07-04', '2020-07-10', '1593241102.jpg', 'Open Jastip\r\n\r\nOkay kita open jastip untuk skin care, hair care dll. buat yang mau kita lagi ada barang-barang skin care terbaru hair care juga terupdate. bisa langsung hubungin kita yaa', 1, '2020-06-20 23:30:42', '2020-07-05 15:00:00'),
(40, 15, 'Alam Sutra, Jakarta', 'Jakarta Selatan', '2020-07-06', '2020-07-11', '1592816419.jpg', 'Hello Everybody, Jastip jastipp\r\n\r\nNah kali ini kita ngadain khusus jastip IKEA di alam sutra jakarta selatan, nah buat kalian yang mau nitip barang-barang kebutuhan rumah bisa banget makek jasa kami yaa, langsung bisa hubungin kontak yang tersedia,, yeayyy\r\n\r\nThank You', 1, '2020-06-22 00:48:49', '2020-07-05 15:52:52'),
(41, 16, 'Gatot Subroto, Denpasar', 'Bali', '2020-07-05', '2020-07-10', '1593241258.jpg', 'Jastip Aman by Yogi!!\r\n\r\nHallo, jastip kali ini kita ngadain jastip di Informa Gatsu, nah yang masih binggung mau beli barang tanpa harus ke toko, bisa banget makek jasa titip by yogi yaa, kali nitip barang apa aja bisa. Jangan sampek ketinggalan jastip kali ini.\r\n\r\nSee You', 1, '2020-06-22 01:09:55', '2020-07-05 15:53:49'),
(42, 1, 'Jakarta Pusat', 'DKI Jakarta', '2020-07-01', '2020-07-08', '1593243479.jpeg', 'Hai Hai,,\r\n\r\nNitipnyadiDiah kali ini open jastip di Jakarta ya, nah buat kalian yang udah kangen sama barang-barang Jakarta, makanan Jakarta, bisa banget nih makek jasa penitipan kita. yuk yang gak mau ketinggalan sama slot jastip kali ini, jangan sampek ketinggalana yaaa,\r\n\r\nluv', 1, '2020-06-26 23:38:00', '2020-07-05 15:54:56'),
(43, 11, 'Tengku Umar, Denpasar', 'Bali', '2020-07-01', '2020-07-07', '1593258664.jpg', 'Helloo,\r\n\r\nJastip kali ini khusus ya di level 21 mall, nah buat kalian yang mau jastip boleh banget. Makanan dan minumannya yang banyak varian bisa banget buat jastip, nah selain makanan dan minuman, ada pakaian, accesoris, parfum dan masih banyak lagi.\r\nYuk buruan jastip, jangan sampek kelewatan yaahh', 1, '2020-06-27 03:51:04', '2020-07-05 15:55:41'),
(44, 17, 'Sanur, Denpasar', 'Bali', '2020-07-02', '2020-07-07', '1593667505.jpg', 'kita akan membuka jasa titip di sanur, buat yang mau jastip jangan sampek kelewatan', 0, '2020-07-01 21:25:06', '2020-07-05 14:33:39'),
(45, 18, 'Denpasar', 'Bali', '2020-07-06', '2020-07-10', '1594017694.jpg', 'Matahari Duta Plaza\r\n\r\nHi buat kalian yang mau ikut jastip dari kita, bisa langsung hubungin contact kita ya', 1, '2020-07-05 22:41:35', '2020-07-05 22:41:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `members`
--

CREATE TABLE `members` (
  `id_member` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `status_jastip` enum('diterima','belum') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `members`
--

INSERT INTO `members` (`id_member`, `username`, `password`, `nama`, `alamat`, `kota`, `provinsi`, `email`, `no_hp`, `nik`, `foto`, `keterangan`, `status`, `admin_id`, `status_jastip`, `created_at`, `updated_at`) VALUES
(1, 'NitipnyadiDiah', '$2y$10$OYIbCr3orh/xHA1ojA.Z5eGuPXKCCI9BPyZQyyLVpTn4KTpiwdb8u', 'Luh Diah Sue Febriani', 'jalan tukad badung 1', 'Denpasar', 'Bali', 'diahsfebriani@gmail.com', '081992335447', '507788', '1586317391.jpg', 'Belum Menikah', 1, 1, 'diterima', '2020-02-12 23:41:33', '2020-07-05 15:54:56'),
(2, 'ana72Jastip', '$2y$10$rF9.SnFBbF4ws7b.SoMThuqaYTrFs69BytHvwxL27.FxOoyK8EJPS', 'Ana Febri Wijaya', 'jalan banyubiru negara', 'Negara', 'Bali', 'wijaya_febriana@yahoo.com', '082247771899', '45678', '1585625826.jpg', 'Menikah', 1, 1, 'diterima', '2020-02-13 01:52:33', '2020-07-05 14:35:26'),
(3, 'milariOnline', '$2y$10$CCDWFbWdLAx/e1uwMtokSuyoD3fsJcFF3oxyw3nRdp5LdYY.SUulu', 'Putu Milari Sari', 'jakarta pusat', 'Badung', 'Bali', 'milari@gmail.com', '081573884992', '12345', '1585625927.jpg', 'Menikah', 1, 1, 'belum', '2020-02-13 01:57:32', '2020-07-05 14:36:09'),
(4, 'LiaPenitipan', '$2y$10$THBW8JVK6X.TJRo1YgIXjeHV1ztA70kQsnvfj8vqtwwi75bY1AFHC', 'Ni Kadek Aprilia', 'jl jendral basuki rachmat', 'Karangasem', 'Bali', 'aprilia@gmail.com', '081224678901', '5099707', '1585626073.png', 'Belum Menikah', 1, 1, 'belum', '2020-02-13 02:00:58', '2020-07-05 14:27:25'),
(5, 'nitipbarangbisa', '$2y$10$yZ8HMBoUodDpYxKLQLVbN.MSOUklMAnSTiBYUhEXJKBJHAqagf.Ke', 'Putu Novik Rahayu', 'Jalan Sudirman No. 130 Bandung, Jawa Barat', 'Denpasar', 'Bali', 'novikrahayu23@gmail.com', '087335912457', '50034501', '1585626415.jpg', 'Belum Menikah', 1, 1, 'belum', '2020-02-13 02:09:53', '2020-07-05 14:37:02'),
(6, 'bisaNitipApapun', '$2y$10$ExEgO5VuEPejo8wI7oynBuMAFHxyDli7ICVR8HzdWVqkJRZvfUiS2', 'Ode Mega Dewi', 'Jalan Raya Tabanan No. 470', 'Tabanan', 'Bali', 'odemega11@gmail.com', '081278446963', '50080901', '1585626639.jpg', 'Belum Menikah', 1, 1, 'belum', '2020-02-13 02:13:51', '2020-07-05 14:38:06'),
(7, 'nitipBarengLoly', '$2y$10$kWTbEPf.rR3tpB9T2I2dceJ.qTkZP7RdiompJVcdzyxfkrEhB.Fqy', 'Komang Lolyta Putri', 'jalan wisnu kencana tabanan', 'Jakarta', 'DKI Jakarta', 'lolyputri98@gmail.com', '089764696098', '5009668', '1585627144.jpg', 'Belum Menikah', 1, 1, 'diterima', '2020-02-13 10:03:15', '2020-07-05 14:39:10'),
(8, 'JastipnyaMus', '$2y$10$z2OTaHlS.Y3J7W/TwvMXu.UqaYVCxDnkTPfNIDE/n3u5eglm7ferS', 'Putu Mustari Oka', 'Jalan Astina Pura Gianyar', 'Gianyar', 'Bali', 'mustarioka3@gmail.com', '081999803989', '50100789', '1586872638.png', 'Belum Menikah', 1, 1, 'diterima', '2020-04-14 05:57:19', '2020-07-05 15:36:52'),
(9, 'PenitipanAyuDevi', '$2y$10$cGeQNA2NVzD6zCtdBK46l.e8HRThHc849mrT3efouVuvilncq.u26', 'Made Ayu Devi', 'Jalan Wisnu Marga', 'Tabanan', 'Bali', 'ayudevi15@gmail.com', '087915114999', '50501123', '1586872843.jpg', 'Belum Menikah', 1, 1, 'belum', '2020-04-14 06:00:43', '2020-07-05 16:09:41'),
(10, 'SriadnyaniJualan', '$2y$10$yKT4jR5pXSer32Q85itMEeYKQdiS8KXKv3ZsAQ/QN3oVqpzS.yLzi', 'Ni Ketut Sri Adnyani', 'Jalan Wr. Supratman', 'Denpasar', 'Bali', 'sriadnyani@gmail.com', '089133145567', '50801917', '1586873001.jpg', 'Menikah', 1, 1, 'diterima', '2020-04-14 06:03:21', '2020-07-05 14:24:28'),
(11, 'NitipSamaArdina', '$2y$10$3Oxsxcz4RT.WiMXaKww/5eP9GMCqv3mZOfGknRF9c5zwBnOQuFbnG', 'Ardina Candra Wulan', 'Jalan, Batanghari 3 No.11', 'Denpasar', 'Bali', 'ardinacandrawln@gmail.com', '081737634112', '500119', '1588422167.jpg', 'Belum Menikah', 1, 1, 'diterima', '2020-05-02 04:22:47', '2020-07-05 15:55:41'),
(12, 'Jastipyogaprtm', '$2y$10$bRtlZM3ANm6XWDSU//5YveEaclrKXzfLTDkGTfvJxvFf9udWQj/Pe', 'Gede Yoga Pratama', 'Jalan, Suputan No.15', 'Kelungkung', 'Bali', 'yogaprtm@gmail.com', '081999313445', '501076', '1588422983.jpg', 'Belum Menikah', 1, 1, 'diterima', '2020-05-02 04:36:23', '2020-07-05 14:25:34'),
(13, 'NitipBersamaSurya', '$2y$10$8yCdjmIPWNmVsW96vl210ejPbOMABH2RW.gsV7RZqxB5TWOKeUuZa', 'Rully Artono Surya', 'Jalan, Dewi Sri 4 No.45', 'Kuta', 'Bali', 'rullysurya@gmail.com', '081987435678', '500114', '1588423360.png', 'Belum Menikah', 1, 1, 'diterima', '2020-05-02 04:42:40', '2020-07-05 14:26:40'),
(15, 'NashitaJastip', '$2y$10$7NgKODrr/OEBapMMr.2pBOPWd0dh9fBmwxBZShAKkxyYEa0Fe0XEi', 'Putu Nashita Candani Ayu', 'Jalan, Udayana no 35, Negara', 'Negara', 'Bali', 'nashitacandani@gmail.com', '081999734568', '5005345', '1589426217.jpg', 'Belum Menikah', 1, 1, 'diterima', '2020-05-13 19:16:57', '2020-07-05 15:52:52'),
(16, 'Jastip Aman by Yogi', '$2y$10$d70gWyt/gU9gGhU.rNfGyeEDdZcGpccrw.bD6Bvj5ADYbKuIri2s2', 'Made Yogi Prayoga', 'Jalan Brigjen Ngurah Rai, no.110', 'Bangli', 'Bali', 'madeYogi@gmail.com', '081798665421', '5001789', '1589426771.png', 'Belum Menikah', 1, 1, 'diterima', '2020-05-13 19:26:11', '2020-07-05 15:53:49'),
(17, 'SharaDevitaNitip', '$2y$10$IK6TsWBMx1BlNVimobSXAOAmFE320FpQjXlc0kcuw41FJYT0YQAfC', 'Gusti Ayu Shara Devita', 'Jalan Tukad Pancoran X, gang mawar 1 No. 12', 'Denpasar', 'Bali', 'sharadevita@gmail.com', '087913655447', '5001234', '1592817189.png', 'Belum Menikah', 1, 2, 'belum', '2020-06-22 01:13:10', '2020-07-05 14:33:39'),
(18, 'siskaJastip', '$2y$10$00WkKAJYOrHK7amWMArkbOk5t.REP93ShiEJjTk.E.q8udorG5LRW', 'Siska', 'Jalan Raya Renon, no 123 Denpasar', 'Denpasar', 'Bali', 'siska12@gmail.com', '085234675867', '5010123', '1594017525.png', 'Belum Menikah', 1, 2, 'diterima', '2020-07-05 22:38:45', '2020-07-05 22:41:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_09_000251_create_admins_table', 1),
(4, '2020_02_09_000529_create_members_table', 1),
(5, '2020_02_09_000544_create_jastips_table', 1),
(6, '2020_02_09_000554_create_ratings_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ratings`
--

CREATE TABLE `ratings` (
  `id_rating` bigint(20) UNSIGNED NOT NULL,
  `member_id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `komentar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ratings`
--

INSERT INTO `ratings` (`id_rating`, `member_id`, `nama`, `email`, `rating`, `komentar`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Ita Diana', 'itadiana10@gmail.com', 4, 'Puas banget sama jastipnya Nitipnya diDiah, pelayanan yang cepat dan barang yang di cari sesuai', 1, '2020-04-19 03:40:11', '2020-04-19 03:40:11'),
(3, 1, 'Citra Utami', 'citrautm23@gmail.com', 5, 'Suka banget nitip barang di Nitipnya di Diah bener-bener pelayanannya bagus dan cepet, barang yang kita request sesuai sama yang kita pesen dan kondisi barang yang kita beli selamat pas udah di ambil', 1, '2020-05-08 04:09:56', '2020-05-08 04:09:56'),
(4, 2, 'Intan Purnama', 'intanpurnama@gmail.com', 5, 'Bahagia banget barang titipan yang aku pengen udah sampek, pas sesuai yang aku pengen dan pelayananya bagus banget', 1, '2020-05-09 23:08:47', '2020-05-09 23:08:47'),
(5, 2, 'Widyani Ayu', 'widyaayu@gmail.com', 4, 'Seneng banget ketemu jasa penitipan dari ana72jastip, barang yang aku cari-cari akhirnya dapet juga. Suka deh sama pelayanan ana72jastip ini.', 1, '2020-05-09 23:13:18', '2020-05-09 23:13:18'),
(6, 3, 'Putri Jesika', 'jesikaptr@gmail.com', 5, 'Kemaren aku nitip apple watch sama milari sari, kebetulan banget aku nitip barang sama milari dapet barangnya cepet dan langusng di proses jadinya gak perlu nunggu lama', 1, '2020-05-09 23:18:25', '2020-05-09 23:18:25'),
(7, 4, 'Melda Darmika', 'meldadrmk@gmail.com', 5, 'Jujur suka banget ada jastip dari Lia Penitipan, jadi gampang mau nitip barang yang dipengen. lebih praktik ada jastip ini beli pakaian seragam kelurga jadi lebih mudah. Terimakasih', 1, '2020-05-09 23:43:31', '2020-05-09 23:43:31'),
(8, 4, 'Nadia Sukma Lestari', 'nadnadia9@gmail.com', 4, 'Kemaren aku nitip beli Perhiasan Mas gampang banget barangnya dateng juga aman suka deh. Terimakasih lia penitipan', 1, '2020-05-09 23:46:10', '2020-05-09 23:46:10'),
(9, 6, 'Utik Putri', 'utikptr@gmail.com', 5, 'Jujur seneng banget ada jasa penitipan ini beli apapun jadi gampang, beli sayur-sayuran gampang dan masak jadi lebih cepet', 1, '2020-05-09 23:51:29', '2020-05-09 23:51:29'),
(10, 6, 'Made Surya Arta', 'suryaarta14@gmail.com', 5, 'Aku salah satu pelanggan Warung Made, sekarang mau beli apa-apa diwarung made jadi enak, pesen pizza mudah praktis', 1, '2020-05-09 23:53:31', '2020-05-09 23:53:31'),
(11, 6, 'Wirda Utami', 'wirdautami@gmail.com', 4, 'Bener-bener enak banget ada jasa penitipan ini jadinya beli buah-buahan segar  sama sayur segar dari baturiti lebih gampang dan nyampeknya cepet', 1, '2020-05-10 00:00:15', '2020-05-10 00:00:15'),
(12, 7, 'Maya Melia Mutiara', 'mayamutiara@gmail.com', 5, 'Pokoknya mau seneng banget ada jasa penitipan ini jadinya mau beli makanan gak perlu keluar rumah lagii', 1, '2020-05-10 00:10:47', '2020-05-10 00:10:47'),
(13, 7, 'Ningrat Laksmi Pemuda', 'laksmingrt21@gmail.com', 4, 'Salah satu pencinta makanan tapi mager keluar buat beli, nah ada penitipan ini lebih mudah buat cari makanan yang disuka', 1, '2020-05-10 00:15:05', '2020-05-10 00:15:05'),
(14, 5, 'Kartika Kumala Wati', 'kartikakw@gmail.com', 5, 'Seneng banget banget ada yang ngadain jastip di Sanur, udah ngiler banget sama nasi Bali Men Weti. akhirnya bisa beli karna nitip di penitipan by Novik. Terimakasih Jastip by Novik', 1, '2020-05-10 05:52:33', '2020-05-10 05:52:33'),
(15, 5, 'Bella Ratna Santoso', 'bellabel@gmail.com', 4, 'Udah pada ngiler sama Rujak Sanur Men Runtu seneng banget ada jasa titip makin enak buat belanja tanpa ribet dan mudah pastinya. Sukses selalu jastip by Novik', 1, '2020-05-10 05:55:13', '2020-05-10 05:55:13'),
(16, 5, 'Della Ayu Kesuma', 'ayudella4@gmail.com', 5, 'Serba gambang berkat Jastip by Novik, sekarang mau nitip apapun gampang banget dan pastinya kondisi makanan yang kita pesan masih enak buat di makan', 1, '2020-05-10 05:57:44', '2020-05-10 05:57:44'),
(17, 1, 'Nila Rastanty', 'nilarastanty@gmail.com', 5, 'Seneng banget sama jastip by Diah jujur kemaren aku jastip buat beli kado karna aku gak sempet, dan barang yang aku order sesuai sama yang aku mau, nyampek barangnya juga cepet bagus deh pokoknyaa', 1, '2020-05-10 06:03:07', '2020-05-10 06:03:07'),
(18, 1, 'Devi Agustini', 'deviagustini@gmail.com', 5, 'Lagi ngincer Tas hight hand branded dan aku titip di Nitipnya di Diah karna pelayanannya bener bagus banget, barang yang aku mau selalu sesuai, dan ini udah orderan jastip kedua di Nitipnya di Diah . Sukses Teruss kakakkk', 1, '2020-05-10 06:06:41', '2020-05-10 06:06:41'),
(19, 1, 'Shalsa Agustia', 'shalsaagustia@gmail.com', 5, 'Sebahagia itu ada jasa penitipan ini mau nitip barang jadi mudah banget. kemaren nitip sepatu akhirnya dapet juga berkat jastipnya Nitipnya di Diah', 1, '2020-05-11 22:18:01', '2020-05-11 22:18:01'),
(20, 1, 'Shinta Imel', 'shintamel22@gmail.com', 5, 'Akhirnya aku bisa dapet Tas impian aku, terimakasih Nitipnya di Diah', 1, '2020-05-11 22:22:20', '2020-05-11 22:22:20'),
(21, 4, 'Reza Arnita', 'rezaarnita@gmail.com', 5, 'akhirnya 1 set keben yang aku pesen udah nyampek terimaksihh jastip by Lia', 1, '2020-05-11 22:28:54', '2020-05-11 22:28:54'),
(22, 4, 'Linda Kumala', 'lindakumala27@gmail.com', 5, 'Titipan seragam kebaya aku udah masukk akhirnyaaaa terimakasihhhh', 1, '2020-05-11 22:31:02', '2020-05-11 22:31:02'),
(23, 4, 'Ari Darma Wati', 'darmawati@gmail.com', 4, 'Nitip beli perhiasan cepet aman banget, barang sesuai sama apa yang aku order', 1, '2020-05-11 22:33:03', '2020-05-11 22:33:03'),
(24, 4, 'Juli Artawan', 'julijul3@gmail.com', 5, 'Gak pakek ribet karna ada jasa titip by lia, 1 set udeng idaman udah nyampek dengan aman dirumahh. Terimakasihh luv luv', 1, '2020-05-11 22:35:40', '2020-05-11 22:35:40'),
(25, 4, 'Gangga Pradnya', 'ganggapradnya@gmail.com', 4, 'Seneng gampang mudah sekarang ada jastip ini aku jadi gampang bisa beli apapun, seragaman kain kebaya aku udah di rumahh', 1, '2020-05-11 22:44:15', '2020-05-11 22:44:15'),
(26, 2, 'Sukma Ningtias', 'sukmatias11@gmail.com', 5, 'Terimakasihh Ana jastip belanja lebih mudah dan cepat', 1, '2020-05-11 22:47:15', '2020-05-11 22:47:15'),
(27, 2, 'Welsa Kumala', 'welsakumala@gmail.com', 5, 'Seneng sesenengnya bisa punya celana yang udah aku incer, berkat ana jastip semua jadi mudah', 1, '2020-05-11 23:01:51', '2020-05-11 23:01:51'),
(28, 2, 'Fitri Susanti', 'fitrisusanti@gmail.com', 5, 'Nitip  makanan jadi mudah, praktis tanpa ribet makasii', 1, '2020-05-11 23:04:27', '2020-05-11 23:04:27'),
(29, 2, 'Andre  Sia', 'andresia@gmail.com', 5, 'Lebih gampang berkat jastip ini, beliin kado buat pacar jadi sesuai sama yang dia mau', 1, '2020-05-11 23:26:09', '2020-05-11 23:26:09'),
(30, 2, 'Agus Suadnyana', 'agussudanyana@gmail.com', 5, 'Terimakasih ana jastip berkat jastip ini pacar aku jadi bahagia karna dapet barang yang dia mau', 1, '2020-05-11 23:31:08', '2020-05-11 23:31:08'),
(31, 2, 'Sayu Lisa', 'sayuLisa26@gmail.com', 5, 'Bener bener dimudahkan sekarang berkat jastip ini belanjar apapun terasa mudah dan gampang', 1, '2020-05-11 23:34:13', '2020-05-11 23:34:13'),
(32, 2, 'Darma Yanthi', 'darmaYanthi17@gmail.com', 5, 'Kebetulan Skin Care udah mulai habis, berkat jastip by ana jadi lebih mudah buat belanja', 1, '2020-05-11 23:39:22', '2020-05-11 23:39:22'),
(33, 4, 'Wulan Pradnyani', 'wlnpradnyani@gmail.com', 5, 'Terimakasih ya berkat jastipnya Aprilia saya jadi gampang mau  1 set perhiasan emat untuk upacara anak saya, pelayanannya cepet dan bagusss', 1, '2020-05-12 17:03:20', '2020-05-12 17:03:20'),
(34, 9, 'Kadek Andresia Lukman', 'andre2001@gmail.com', 5, 'Pelayanannya cepet banget,  ikut jastip tanggal 21, besokknya udah ready barang yang aku mau', 1, '2020-06-20 23:33:23', '2020-06-20 23:33:23'),
(35, 9, 'Made Dwi Kesuma', 'dwikesuma@gmail.com', 5, 'seneng banget persiapan kado buat pacar aku udah siap buat tanggal 23, pelayannya cepet banget', 1, '2020-06-20 23:34:45', '2020-06-20 23:34:45'),
(36, 9, 'Luh Anik Yanti', 'anikyanti19@gmail.com', 5, 'Nitip beli parfum udah ready aja orderanku, ah suka deh', 1, '2020-06-20 23:36:17', '2020-06-20 23:36:17'),
(37, 9, 'Dayu Kumara Santi', 'kumarasanti@gmail.com', 5, 'Baju yang aku incer akhirnya dapet juga, makasi banyak ya kak', 1, '2020-06-20 23:37:44', '2020-06-20 23:37:44'),
(38, 3, 'Putri Sapta Agustin', 'ptrAyu@gmail.com', 5, 'Kesampean juga mau makan makanan khas gianyar, suka deh', 1, '2020-06-20 23:39:46', '2020-06-20 23:39:46'),
(39, 11, 'Tirta Wahyu Dewata', 'titrawahyu26@gmail.com', 5, 'seneng banget ada jasa titip ini aku nitip beli mesin traktor kakek aku, jadi lebih gampangg dan lebih cepet pokoknya', 1, '2020-06-20 23:41:42', '2020-06-20 23:41:42'),
(40, 11, 'Made Sukma Dewi', 'madesukma@gmail.com', 5, 'Aku nitip beli sepeda akhirnya dapet juga, yang pasti harganya lebih murah', 1, '2020-06-20 23:43:45', '2020-06-20 23:43:45'),
(41, 11, 'Putu Dinda Andini', 'dindaandini@gmail.com', 5, 'Kemaren aku nitip beli mesin parutan kelapa kecil akhirnya dapet jugaa, berkat jasa titip ini. luv luv deh', 1, '2020-06-20 23:45:57', '2020-06-20 23:45:57'),
(42, 1, 'Putu Maha Pratiwi', 'mahaprtw@gmail.com', 5, 'bener-bener berkat ada jastip ini merasa praktis karna mudah buat beli apa\", dan barang yang kita mau nyampek samapek dirumah', 1, '2020-06-25 16:00:34', '2020-06-25 16:00:34'),
(43, 1, 'Made Suriati', 'madesuri6@gmail.com', 5, 'awalnya ragu mau jastip kain ini, karna bener harganya mahal, tapi ternyata kualitasnya gak diraguiin lagi pelayanannnya ok banget dan cepet. makasi ya', 1, '2020-06-25 16:03:21', '2020-06-25 16:03:21'),
(44, 1, 'Komang Tio Hermawan', 'tiohermawan@gmail.com', 5, 'Gampang banget ada jastip ini, beliin ibu kado jadi gampang dengan barang yang diingin, udh nyampek rumah dengan selamat', 1, '2020-06-25 16:05:37', '2020-06-25 16:05:37'),
(45, 9, 'Luh Ayu Intania', 'ayuintan@gmail.com', 4, 'gak mengecewain aja jastip ini, karna belanja jadi gampang tanpa perlu keluar rumah', 1, '2020-06-25 16:08:21', '2020-06-25 16:08:21'),
(46, 9, 'Pitrofika Wahyuni', 'pitrofwahyuni@gmail.com', 5, 'Bahagianyaa belanja mudah dan nyaman pastinya. Terimakasih', 1, '2020-06-25 16:19:28', '2020-06-25 16:19:28'),
(47, 9, 'Putu Govinda Permana', 'govindaPrmn@gmail.com', 5, 'Pelayanannya cepat ya, jadi gampang belanja dan barang nyampek dengan selamat', 1, '2020-06-25 16:22:27', '2020-06-25 16:22:27'),
(48, 9, 'Cika Christina', 'cikacika12@gmail.com', 5, 'Wahhh gampang banget sekarang belanja tanpa harus keluar rumah dan barang aku nyampek dengan selamat', 1, '2020-06-25 16:42:52', '2020-06-25 16:42:52'),
(49, 10, 'Mae Ayu', 'mdayu@gmail.com', 5, 'Makasi ya gak perlu ke pasar lagi tapi bisa belanja', 1, '2020-06-25 16:44:19', '2020-06-25 16:44:19'),
(50, 10, 'Kadek Suwitri', 'switri1989@gmail.com', 5, 'Gampang ya sekerang gak pelru ke pasar tinggal pesen udah nyampek dirumah', 1, '2020-06-25 16:45:39', '2020-06-25 16:45:39'),
(51, 10, 'Kadek Dwi Darmayanti', 'dwidarma@gmail.com', 5, 'Aduhhh lebih gampang belanja kayak gini, karna tinggal diem dirumahh', 1, '2020-06-25 16:47:20', '2020-06-25 16:47:20'),
(52, 10, 'Ketut Ari Susanti', 'ariSusanti@gmail.com', 5, 'wahh makasi banyak jastipnya lebih mudah buat kerja karna tanpa kepasar', 1, '2020-06-25 16:49:10', '2020-06-25 16:49:10'),
(53, 10, 'Made Muli Aryanti', 'muliMuli@gmail.com', 4, 'Titip beli bahan-bahan masak jadi gampang sekarang', 1, '2020-06-25 16:50:29', '2020-06-25 16:50:29'),
(54, 10, 'Murniati Sukma', 'murniSukma@gmail.com', 4, 'Belanja apapun lebih mudan dan aman tentunya', 1, '2020-06-25 16:51:35', '2020-06-25 16:51:35'),
(55, 10, 'Mareta Aryani', 'mareta0503@gmail.com', 4, 'Kemudahan belanja kayak gini emang the best', 1, '2020-06-25 16:54:33', '2020-06-25 16:54:33'),
(56, 10, 'Yovita Pramasudi', 'yovita25@gmail.com', 5, 'Belanja kayak gini lebih mudah, bahan-bahan dapur habis tinggal pesen aja udah nyampek rumah', 1, '2020-06-25 16:56:14', '2020-06-25 16:56:14'),
(57, 10, 'Desak Muliani', 'desakMuli@gmail.com', 5, 'Sekarang belanja apa-apa jadi mudah', 1, '2020-06-25 17:08:38', '2020-06-25 17:08:38'),
(58, 10, 'Made Nilawati', 'nilawati@gmail.com', 5, 'Terimakasi banyak berasa dimudahkan belanja kayak gini', 1, '2020-06-25 17:10:00', '2020-06-25 17:10:00'),
(59, 11, 'Putu Dana Mahardika', 'danamahardika@gmail.com', 5, 'Nitip beliin ana sepeda lebih mudah dan harganya jauh lebih terjangkau', 1, '2020-06-25 17:13:24', '2020-06-25 17:13:24'),
(60, 11, 'Made Sudiatmika', 'sudiatmika66@gmail.com', 5, 'beli dinamo parutan kelapa jadi mudah, karna nyari di daerahku susah, untuk ada jastip ini, semua jadi mudah', 1, '2020-06-25 17:15:19', '2020-06-25 17:15:19'),
(61, 11, 'Gede Yuni Antara', 'gedeYuni@gmail.com', 5, 'nitip beli alat-alat mesin dan peralatan sepeda jadi mudah', 1, '2020-06-25 17:16:55', '2020-06-25 17:16:55'),
(62, 11, 'Ketut Suwidiaya', 'suwidiyasa@gmail.com', 5, 'bener-bener dimudahin sekarang apa-apa serba online', 1, '2020-06-25 17:19:05', '2020-06-25 17:19:05'),
(63, 11, 'Kadek Moleh Ardini', 'Molehardini@gmail.com', 5, 'beli peralatan buat jualan jadi gampang, nitip beli blender dan oven bener-bener dibuat mudah', 1, '2020-06-25 17:20:49', '2020-06-25 17:20:49'),
(64, 12, 'Nyoman Wawan Prasetya', 'wawanPrsty@gmail.com', 5, 'suksmaa bro, jastipnya luar biasa pelayanannya baguss banget, dan pastinya cepet. bagusss', 1, '2020-06-26 22:55:34', '2020-06-26 22:55:34'),
(65, 9, 'Ajung Taranti Sukma', 'taranti22@gmail.com', 5, 'wahhhh, bahagia banget. gak perlu keluar rumah buat belanja kemeja keperluan kampus. sukaaa', 1, '2020-06-27 03:54:16', '2020-06-27 03:54:16'),
(66, 9, 'Lisa Pratami Sucipta', 'lisaPratami@gmail.com', 4, 'Waduhhh, se seneng ini beli baju buat anak jadi gampangg', 1, '2020-06-27 03:57:14', '2020-06-27 03:57:14'),
(67, 12, 'Made Merta', 'mertaMade@gmail.com', 4, 'Jeg Gampang ajan jani mebelanje beli baju adat pakek kondangan dan ke pura jadi gampang, besok -besok belanja tinggal di hp udah ada belanjaan dateng', 1, '2020-06-27 03:59:36', '2020-06-27 03:59:36'),
(68, 12, 'Ni Luh Ardani', 'luhArdani07@gmail.com', 4, 'Wehhh bahagia banget belanja seragaman acara dirumah dan kostim pakek di banjar, harganya murah dan model kainnya terbaru', 1, '2020-06-27 04:04:45', '2020-06-27 04:04:45'),
(69, 12, 'Made Anjar Pratiwi', 'anjaranjar2@gmail.com', 5, 'Kemaren beli seragaman karna ponakan mau metatah, mudah banget barangnya juga nyampek cepet dan pelayanannya bagus', 1, '2020-06-27 04:08:27', '2020-06-27 04:08:27'),
(70, 12, 'Ni Ketut Mudiati', 'mudiati29@gmail.com', 4, 'Nitip beli kain sama kamen pakek kado, cepet banget nyampeknyaa barangnya juga nyampek dengan selamat\r\nTerimakasih yaa', 1, '2020-06-27 04:12:28', '2020-06-27 04:12:28'),
(71, 12, 'Luh Made Suteni', 'madeSuteni@gmail.com', 5, 'Nitip belin cucu-cucu pakaian adat bagus banget pelayananya, sukses selalu', 1, '2020-06-27 04:14:31', '2020-06-27 04:14:31'),
(72, 13, 'Jielinthan Mahatan', 'jielinJiel@gmail.com', 3, 'Nitip beli Batik  barangnya sesuai sama yang aku mau', 1, '2020-06-27 04:39:10', '2020-06-27 04:39:10'),
(73, 13, 'Purnomo Ahmad', 'purnomo17@gmail.com', 4, 'Terimah kasih kain batik aku sudah nyampek dengan selamat', 1, '2020-06-27 04:42:50', '2020-06-27 04:42:50'),
(74, 13, 'Satria Susanto Putra', 'satriasusanto@gmail.com', 4, 'Ahkirnya pie patok kesukaan aku udah nyampekk, makasi ya', 1, '2020-06-27 04:44:27', '2020-06-27 04:44:27'),
(75, 13, 'Shintia Lan Putri', 'shintiaLan@gmail.com', 5, 'Titipan kain batik aku udah nyampek dengan selamat yeeaayyy', 1, '2020-06-27 04:45:25', '2020-06-27 04:45:25'),
(76, 13, 'Rosita Haryanti', 'rositatata@gmail.com', 5, 'wah kain couple aku udah dirumah', 1, '2020-06-27 04:54:11', '2020-06-27 04:54:11'),
(77, 3, 'Siska Purnama Rahayu', 'siskaPurnama@gmail.com', 4, 'Nitip beli 1 set gelang buat acara anakku udah nyampek, dan gak ada cacat ya karna mereka bawa barangnya hati-hati banget', 1, '2020-06-27 05:18:57', '2020-06-27 05:18:57'),
(78, 3, 'Putu Wirah Sundyca', 'wirahSundyca@gmail.com', 4, '1 Set titipan Alpaka aku udah nyampek aja, cepet banget pelayanannya', 1, '2020-06-27 05:20:02', '2020-06-27 05:20:02'),
(79, 3, 'Komang Intania', 'intania20@gmail.com', 5, 'Seragaman Set Alpaka buat acara nikahan dirumah udah nyampek yeeyyy senengnyaaa', 1, '2020-06-27 05:38:20', '2020-06-27 05:38:20'),
(80, 9, 'Mega Erawati Ayu', 'megaAyu@gmail.com', 5, 'Wahh, Nitip beli sepatu dan flatshoes udah dateng dong', 1, '2020-07-02 04:11:47', '2020-07-02 04:11:47'),
(81, 9, 'Mantri Yosheptyan', 'mantriyoshep26@gmail.com', 4, 'Nitip  beli roti kesukaan orang rumah datengnya cepet banget, gak nyangka bisa ketemu jasa titip ini', 1, '2020-07-02 04:16:15', '2020-07-02 04:16:15'),
(82, 10, 'Putu Wahyuningtyas', 'tyasyas28@gmail.com', 5, 'Gak nyangka aku, dapet recomend dari temen tentang jastip ini, ternyata emang bagus banget pelayanannya, gak nyangka', 1, '2020-07-02 04:19:59', '2020-07-02 04:19:59'),
(83, 12, 'Nandan Pratama Yudi', 'nandanYudi@gmail.com', 4, 'Beli seragaman  seke gong anak-anak  jadi gampang sekarang', 1, '2020-07-02 04:25:46', '2020-07-02 04:25:46'),
(84, 12, 'Rizha Mahardini', 'rizhaDini@yahoo.com', 5, 'Nitip beli wastra pakek di  pura jadi gampang sekarang', 1, '2020-07-02 04:31:33', '2020-07-02 04:31:33'),
(85, 12, 'Wiwik Sekta Putri', 'wiwikSekta4@gmail.com', 4, 'Makasi banyakk, sekarang belanja jadi gampang', 1, '2020-07-02 04:37:47', '2020-07-02 04:37:47'),
(86, 13, 'Maryam Erna Susanti', 'maryaSusan@yahoo.com', 3, 'wahh kebetulan ada jastip di jogja nitip beli salak  pondoh, dan kesampaian buat makan salak pondoh khas jogja', 1, '2020-07-02 04:43:42', '2020-07-02 04:43:42'),
(87, 13, 'Made Dewi Ulan Dewi', 'ulanDewi98@gmail.com', 4, 'Akhirnyaa bisa makan lagi coklat Monggo ini, setalah sekian lama gak kesampaian karna terlalu jauh. Terimakasih', 1, '2020-07-02 05:02:03', '2020-07-02 05:02:03'),
(88, 13, 'Ida Ayu Intan Miranti', 'dayuRanti@gmail.com', 5, 'Kemaren-kemaren cuma  dapet liat postingan di Instagram, dan sekarang bisa makan karna jasa titip ini, wahh seneng banget dong aku tu mah', 1, '2020-07-02 05:04:35', '2020-07-02 05:04:35'),
(89, 13, 'Ketut Arta Wiguna', 'artaWiguna25@gmail.com', 5, 'Akhirnya dapet makan kue Jogja Scrummy dan Pia Batok karna jasa titip ini', 1, '2020-07-02 05:07:28', '2020-07-02 05:07:28'),
(90, 13, 'Luh Evy Damayanthi', 'damaEvy29@gmail.com', 4, 'Kue Mamahke Jogja yang lagi  viral bisa beli tanpa harus pergi ke Jogja', 1, '2020-07-02 05:10:32', '2020-07-02 05:10:32'),
(91, 13, 'Putu Dessy Natalia', 'dessyNata09@gmail.com', 3, 'Beli kain dan baju batik khas Jogja yang  sudah terkenal, Nitip beli pernak-pernik kerajinan jogja juga buat sodara-sodara jadi gampang', 1, '2020-07-02 05:14:49', '2020-07-02 05:14:49'),
(92, 13, 'Putu Elmi Novita', 'elmiNovita@gmail.com', 4, 'Nitip beli Kaos Dagadu buat acara dirumah biar seragam mudah banget', 1, '2020-07-02 05:18:29', '2020-07-02 05:18:29'),
(93, 13, 'Nitya Cinta Purnama', 'nityaCinta20@gmail.com', 4, 'Akhirnya  aku dapet beli barang antik  yang di Jogja vas bunga yang jadi inceran aku akhirnya dapat aku beli', 1, '2020-07-02 05:23:11', '2020-07-02 05:23:11'),
(94, 2, 'Yasmin Yasintha Nugraha', 'yasminYashin@gmail.com', 5, 'Udah gak heran lagi sama jastip ini, karna aku udah nitip sama ana72jastip udah 2x  dan sekarang aku nitip buat beli parfum sama baju di Zara akhirnya dateng juga', 1, '2020-07-02 05:29:26', '2020-07-02 05:29:26'),
(95, 2, 'Wayan Yuda Baskhara Yudi', 'yudaBaskhara@yahoo.com', 4, 'Nitip beli sepatu Addidas buat aku dan  beli baju Guess buat Ibu aku dan sesuai sama yang aku pesenn bahagianyaa', 1, '2020-07-02 05:35:10', '2020-07-02 05:35:10'),
(96, 3, 'Putu Depik Maha Gaori', 'depikmahagao@gmail.com', 5, 'akhirnya ketemu jastip ini, beli set bros silver pakek wisuda jadi gampang, dan pastinya gak buang waktu banyak buat jauh-jauh nyari bros', 1, '2020-07-04 06:06:27', '2020-07-04 06:06:27'),
(97, 3, 'Agung Bayu Permana', 'gungBayu8@gmail.com', 5, 'nitip beli bros model kris, sama cicin permata barang yang aku pesen sesuai sama yang aku mau, jadi mudah kalo belanja kayak gini', 1, '2020-07-04 06:09:54', '2020-07-04 06:09:54'),
(98, 3, 'Gede Dodi Bagusti', 'dodiGD@gmail.com', 4, 'udah gak punya banyak waktu buat beli perlengkapan acara fashion show, dan ketemu sama jastip ini, nitip beli Mahkota yang bagus jadi mudah banget, gak nyangka bisa dimudahin gini sekarang', 1, '2020-07-04 06:13:28', '2020-07-04 06:13:28'),
(99, 3, 'Arindra Putri Mayanthi', 'arinPutri10@yahoo.com', 3, 'Nitip beli Gelang silver akhirnya dapet juga, walaupun nyarinya susah banget. makasi ya jastipnya dari Milari Sari akhirnya aku punya gelang impian ku', 1, '2020-07-04 06:16:21', '2020-07-04 06:16:21'),
(100, 3, 'Arindra Putri Mayanthi', 'arinPutri10@yahoo.com', 3, 'Nitip beli Gelang silver akhirnya dapet juga, walaupun nyarinya susah banget. makasi ya jastipnya dari Milari Sari akhirnya aku punya gelang impian ku', 1, '2020-07-04 06:26:51', '2020-07-04 06:26:51'),
(101, 7, 'Milka Wilonaly', 'milka0711@gmail.com', 5, 'wahh senengnya sekarang mau buat kue jadi gak susah harus pergi ketoko', 1, '2020-07-05 14:41:00', '2020-07-05 14:41:00'),
(102, 7, 'Sabrinna Miranda', 'sabrinna2001@gmail.com', 4, 'nitip beli peralatan buat jualan, baru kemaren titip hari ini barangnya udah sampek', 1, '2020-07-05 14:42:29', '2020-07-05 14:42:29'),
(103, 7, 'Reyhan Agus Ahmad', 'reyhanAgus@gmail.com', 5, 'Nitip beli peralatan hias kue buat persiapan anak sekolah gak perlu ijin keluar kantor karna belanja sekarang serba gampang', 1, '2020-07-05 14:44:38', '2020-07-05 14:44:38'),
(104, 7, 'Firky Lanni Mas', 'masLanni@gmail.com', 5, 'Berkat jastip ini aku udah gak perlu keliling nyari loyang  uk 20cm karna selalu kehabisa. dan sekarang jadi mudah karna apa-apa udah tinggal nitip', 1, '2020-07-05 14:47:24', '2020-07-05 14:47:24'),
(105, 7, 'Ulan Destra Karunia', 'destraUlan@gmail.com', 5, 'Bener-bener ya, kalo bisa jastip ini di perpanjanga jujur kebantu banget dengan adanya jastip ini. Thank You', 1, '2020-07-05 14:49:26', '2020-07-05 14:49:26'),
(106, 7, 'Putu Dwiki Pratama', 'dwikiDuk@gmail.com', 5, 'Belanja tepung terigu 20 kg jadi mudah ya sekarang gampang banget buat bantui ibukku belanja bahan-bahan kue', 1, '2020-07-05 14:51:29', '2020-07-05 14:51:29'),
(107, 8, 'Made Harum Herwawati', 'harumHerma@gmail.com', 5, 'Jujur skin care aku udah mula habis, dan ketemu sama jastip ini. senengnyaa', 1, '2020-07-05 14:53:09', '2020-07-05 14:53:09'),
(108, 8, 'Luh Nurul Handayani', 'nurulHandayani@gmail.com', 4, 'Senengnya ada jastip kek gini, beli skin car sama hair care jadi mudah', 1, '2020-07-05 15:01:51', '2020-07-05 15:01:51'),
(109, 8, 'Dewa Ayu Nanda P', 'ayunanda21@gmail.com', 5, 'Wahhh Nitip beli hair care buat ngilangin rambut rontok , gampang banget. padahal nyari shampoo yang aku mau itu harus PO dulu. Tapi berkat jastip dari Mustari semua jadi cepat', 1, '2020-07-05 15:32:54', '2020-07-05 15:32:54'),
(110, 8, 'Made Yumik Suastika', 'yumikMik@gmail.com', 3, 'nyari Scrub Mask face yang aku cari-cari dapet juga. makasi banyakk', 1, '2020-07-05 15:34:55', '2020-07-05 15:34:55'),
(111, 8, 'Anak Agung Istri Mahendrayani', 'agungIstri@gmail.com', 5, 'Gak nyangka bisa ketemu jastip ini, nitip beli skin care tipe mukak sensitif gampang yaa', 1, '2020-07-05 15:40:47', '2020-07-05 15:40:47'),
(112, 8, 'Komang Subagiantara', 'subagiantara@gmail.com', 4, 'Terimakasih Jastip Mustari beli skin care cowok pelayanannya bagus', 1, '2020-07-05 15:42:21', '2020-07-05 15:42:21'),
(113, 8, 'Putu Inggit Sukma', 'ptIngit@gmail.com', 3, 'Memang yaa kalo nitip pasti dapet barangnya, lagi ngincer skincare Korea yang lagi buming, bisa dapet senengnya', 1, '2020-07-05 15:44:34', '2020-07-05 15:44:34'),
(114, 8, 'Putu Elina Pratiwi', 'elinpratiwi@gmail.com', 5, 'Akhirnya rangkaian hair care aku lengkap juga, dan pastinya sekarang udah bisa aku pakek biar dapet hasil yang maximal', 1, '2020-07-05 15:47:11', '2020-07-05 15:47:11'),
(115, 8, 'Made Desi Rahayu', 'desiRahayu@gmail.com', 5, 'Wow wow  seneng deh ketemu jastip ini, mau belanja jadi mudah', 1, '2020-07-05 15:49:54', '2020-07-05 15:49:54'),
(116, 8, 'Komang Sukma Herindrani', 'herindraniskm@gmail.com', 3, 'Skin care aku udah nyampek dong super duper bagus deh', 1, '2020-07-05 15:51:55', '2020-07-05 15:51:55'),
(117, 1, 'Nugraha Adi Puri', 'nugrahaPuri@gmail.com', 4, 'Wow nitip beli sepatu Nike Air Jordan emang udah incer-iceran aku banget, gak nyangka bisa dapet  keluaran terbarunya', 1, '2020-07-05 15:58:34', '2020-07-05 15:58:34'),
(118, 1, 'Made Ratna Ananda', 'anandaRatna@gmail.com', 3, 'Nitip kue artis yang lagi ngehits di jakarta akhirnya bisa kebeli juga makasi banyak nitipnya di diah', 1, '2020-07-05 16:00:06', '2020-07-05 16:00:06'),
(119, 1, 'Kadek Giofani Chandra', 'giofaniChan@gmail.com', 5, 'Akhirnya tas branded aku nyampek juga dengan selamat, bener-bener pas dateng barangnya aman-aman aja', 1, '2020-07-05 16:05:55', '2020-07-05 16:05:55'),
(120, 11, 'Putu Intan Sakira', 'sakiraIntan@gmail.com', 4, 'wahh, titipan aku super banyak banget oven, Catokan, Make Up, Baju, Tas, Sepatu, Parfum udah nyampek semua akhirnyaa, pelayananya bener-bener gak perlu diraguin lagi', 1, '2020-07-05 16:09:11', '2020-07-05 16:09:11'),
(121, 10, 'Jro Ayu Nilawati', 'jroNilawati@gmail.com', 5, 'Pelayanan dari jastip ini bener\" gak usah diraguin lagi, jujur aku udah sering  nitip sama tut Sri memang bagus ya, barang yang aku pesen selalu sesuai', 1, '2020-07-05 16:11:35', '2020-07-05 16:11:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `jastips`
--
ALTER TABLE `jastips`
  ADD PRIMARY KEY (`id_jastip`);

--
-- Indeks untuk tabel `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id_member`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id_rating`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admins`
--
ALTER TABLE `admins`
  MODIFY `id_admin` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `jastips`
--
ALTER TABLE `jastips`
  MODIFY `id_jastip` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT untuk tabel `members`
--
ALTER TABLE `members`
  MODIFY `id_member` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id_rating` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
