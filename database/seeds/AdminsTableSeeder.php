<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username' => 'diah',
            'password' => bcrypt('123456'),
            'nama' => 'diah',
            'alamat' => 'jalan tukad badung',
            'email' => 'diah123@gmail.com',
            'no_hp' => '081913688449',
            'keterangan' => 'mahasiswa',
            'status' => '1',
            'created_at'=> now(),
            'updated_at'=> now(),
         ]);
    }
}
