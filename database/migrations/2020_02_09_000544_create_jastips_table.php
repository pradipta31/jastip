<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJastipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jastips', function (Blueprint $table) {
            $table->bigIncrements('id_jastip');
            $table->unsignedBigInteger('member_id')
                    ->foreign('member_id')->references('id_member')->on('members');
            $table->string('kota');
            $table->string('provinsi');
            $table->date('tanggal_awal');
            $table->date('tanggal_akhir');
            $table->string('foto');
            $table->text('keterangan');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jastips');
    }
}
