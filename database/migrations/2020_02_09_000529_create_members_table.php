<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id_member');
            $table->string('username',25);
            $table->string('password',60);
            $table->string('nama',50);
            $table->text('alamat');
            $table->string('kota');
            $table->string('provinsi');
            $table->string('email',50);
            $table->string('no_hp',15);
            $table->string('nik',25);
            $table->string('foto');
            $table->text('keterangan');
            $table->integer('status');
            $table->unsignedBigInteger('admin_id')
                    ->foreign('admin_id')->references('id_admin')->on('admins');
            $table->enum('status_jastip',['diterima','belum']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
