<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>AdminLTE 3 | Top Navigation</title>
  @toastr_css
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css">
  <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('backend/rating.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  @include('pengunjung.layouts.navbar')
    <div class="content-wrapper" style="background-image: url('images/gallery/gambar2.jpeg'); repeat: no-repeat">
      @include('pengunjung.layouts.breadcrumb')
      <div class="content">
        <div class="container">
          @yield('frontend')
        </div>
      </div>
    </div>
  @include('pengunjung.layouts.footer')
</div>
    @jquery
    @toastr_js
    @toastr_render
<script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
@yield('js')
</body>
</html>
