<nav class="main-header navbar navbar-expand-md navbar-dark navbar-lightblue">
    <div class="container">
      <a href="index3.html" class="navbar-brand">
        <img src="{{asset('images/gallery/logo.jpeg')}}" alt="Jastip Logo" class="brand-image img-circle elevation-3">
        {{-- <span class="brand-text font-weight-light">Jastip</span> --}}
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="{{url('/')}}" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="{{url('jastip')}}" class="nav-link">Jasa Titip</a>
          </li>

          <li class="nav-item">
            <a href="{{url('contact/us')}}" class="nav-link">Contact Us</a>
          </li>
        </ul>
      </div>

    </div>
  </nav>
  <!-- /.navbar -->