@extends('pengunjung.layouts.master')
@section('breadcrumbs', 'Detail Jastip '.$jastip->kota)
@section('frontend')
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title m-0">
                        <i class="fa fa-map"></i>
                        Wilayah: {{$jastip->kota}}, {{$jastip->provinsi}}
                        <small style="font-weight:10"> {{date('d M, Y', strtotime($jastip->tanggal_awal))}} - {{date('d M, Y', strtotime($jastip->tanggal_akhir))}}</small>
                    </h1>
                </div>
                <div class="card-body">
                    <img src="{{asset('images/jastip/'.$jastip->foto)}}" alt="" height="300px" width="100%">
                    <hr>
                    <p class="card-text">
                        <i class="fa fa-tag"></i>
                        {{$jastip->keterangan}}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title m-0">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="{{asset('images/member/'.$jastip->member->foto)}}" alt="" class="img-circle img-fluid" height="175px" width="175px">
                            </div>
                            <div class="col-lg-8">
                                <h5>{{$jastip->member->nama}} </h5>
                                <small>Member</small>
                            </div>
                        </div>
                    </h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            Nama
                            <br>
                            Email
                            <br>
                            Alamat
                            <br>
                            Kota
                            <br>
                            Provinsi
                            <br>
                            No HP
                            <br>
                            NIK
                            <br>
                            Keterangan
                        </div>
                        <div class="col-lg-8">
                            : {{$jastip->member->nama}}
                            <br>
                            : {{$jastip->member->email}}
                            <br>
                            : {{$jastip->member->alamat}}
                            <br>
                            : {{$jastip->member->kota}}
                            <br>
                            : {{$jastip->member->provinsi}}
                            <br>
                            : {{$jastip->member->no_hp}}
                            <br>
                            : {{$jastip->member->nik}}
                            <br>
                            : {{$jastip->member->keterangan}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title m-0">
                        <h5>Review From Our Customer</h5>
                    </h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($ratings as $rating)
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <h5>
                                        <i class="fa fa-comment"></i>
                                        {{$rating->email}} <small>({{$rating->nama}})</small></h5>
                                    <p>{{$rating->komentar}}</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                @if($rating->rating == 1)
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                @elseif($rating->rating == 2)
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                @elseif($rating->rating == 3)
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                @elseif($rating->rating == 4)
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                @elseif($rating->rating == 5)
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                    <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                @endif
                            </div>
                            <div class="col-lg-12"><hr></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title m-0">
                        <h5>Review Form Member</h5>
                    </h1>
                </div>
                <div class="card-body">
                    <form action="{{url('rating')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="member_id" value="{{$jastip->member_id}}">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama" value="{{old('nama')}}">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}">
                        </div>
                        <div class="form-group">
                            <div class="rating">
                                <label>
                                    <input type="radio" name="rating" value="1" />
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="2" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="3" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>   
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="4" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="5" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                            </div>
                            <br>
                            <small>Berikan penilaian anda (1 - 5).</small>
                        </div>
                        <div class="form-group">
                            <label for="">Komentar</label>
                            <textarea name="komentar" class="form-control" cols="30" rows="5">{{old('komentar')}}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-paper-plane"></i>
                                Kirim
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(':radio').change(function() {
            console.log('New star rating: ' + this.value);
        });
    </script>
@endsection