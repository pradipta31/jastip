@extends('pengunjung.layouts.master')
@section('breadcrumbs', 'Home')
@section('frontend')
<style>
    .card{
        background-color:mintcream;
    }
</style>
    <div class="row">
        @foreach ($jastips as $jastip)
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title m-0">
                            <i class="fa fa-map"></i>
                            Wilayah: {{$jastip->kota}}, {{$jastip->provinsi}}
                            <small style="font-weight:10"> {{date('d M, Y', strtotime($jastip->tanggal_awal))}} - {{date('d M, Y', strtotime($jastip->tanggal_akhir))}}</small>
                        </h1>
                    </div>
                    <div class="card-body">
                        <h6 class="card-title">
                            <i class="fa fa-user"></i>
                            Jastip By: {{$jastip->member->nama}}
                        </h6>
                        <br>
                        <a href="{{url('jastip/detail/'.$jastip->id_jastip)}}">
                            <img src="{{asset('images/jastip/'.$jastip->foto)}}" alt="" height="300px" width="100%">
                        </a>
                        <p class="card-text">
                            <i class="fa fa-tag"></i>
                            {{$jastip->keterangan}}
                        </p>
                        
                        <p class="pull-right">
                            <div class="row">
                                <div class="col-md-9">
                                    <a href="{{url('jastip/detail/'.$jastip->id_jastip)}}" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-calendar-alt"></i>
                                    <small class="pull-right">{{date('d-m-Y', strtotime($jastip->created_at))}}</small>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection