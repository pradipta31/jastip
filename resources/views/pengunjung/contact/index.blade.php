@extends('pengunjung.layouts.master')
@section('breadcrumbs', 'Contact Us')
@section('frontend')
    <div class="row">
        <div class="col-lg-6">
            <img src="{{asset('images/gallery/gambar1.jpeg')}}" alt="" class="img-fluid">
        </div>
        <div class="col-lg-6">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae dignissimos rerum repellat neque veniam quia? Possimus impedit tenetur pariatur, aspernatur neque laboriosam fugit voluptas veritatis quas incidunt, qui nam laborum.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae dignissimos rerum repellat neque veniam quia? Possimus impedit tenetur pariatur, aspernatur neque laboriosam fugit voluptas veritatis quas incidunt, qui nam laborum.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae dignissimos rerum repellat neque veniam quia? Possimus impedit tenetur pariatur, aspernatur neque laboriosam fugit voluptas veritatis quas incidunt, qui nam laborum.
            <hr>
            <div class="row">
                <div class="col-lg-6">
                    <img src="{{asset('images/gallery/gambar3.jpeg')}}" alt="" class="img-fluid">
                </div>
                <div class="col-lg-6">
                    <img src="{{asset('images/gallery/gambar2.jpeg')}}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection