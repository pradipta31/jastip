@extends('admin.layouts.master',['activeMenu' => 'data-jastip'])
@section('title', 'Data Jasa Titip')
@section('breadcrumb', 'Data Jasa Titip')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Jasa Titip</h3>
                </div>
                <div class="card-body">
                    <table id="tableJastip" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Member</th>
                                <th>Kota</th>
                                <th>Provinsi</th>
                                <th>Tanggal Pesan 1</th>
                                <th>Tanggal Pesan 2</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($jastips as $jastip)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$jastip->member->nama}}</td>
                                    <td>{{$jastip->kota}}</td>
                                    <td>{{$jastip->provinsi}}</td>
                                    <td>{{date('d-m-Y', strtotime($jastip->tanggal_pesan_1))}}</td>
                                    <td>{{date('d-m-Y', strtotime($jastip->tanggal_pesan_2))}}</td>
                                    <td>{{$jastip->keterangan}}</td>
                                    <td>
                                        @if($jastip->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#tableJastip').DataTable();
        });
    </script>
@endsection