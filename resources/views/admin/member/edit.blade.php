@extends('admin.layouts.master',['activeMenu' => 'data-member'])
@section('title', 'Edit Member')
@section('breadcrumb', 'Edit Member')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Data Member : {{$member->nama}}</h3>
                </div>
                <form role="form" action="{{url('admin/member/'.$member->id_member.'/edit')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="put">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" placeholder="Masukan Nama" name="nama" value="{{$member->nama}}">
                                </div>
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" class="form-control" placeholder="Masukan NIK" name="nik" value="{{$member->nik}}">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Masukan Password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Re-Type Password</label>
                                            <input type="password" class="form-control" id="confirmation_password" name="confirmation_password" placeholder="Masukan password ulang">
                                            <span id="message"></span>
                                        </div>
                                    </div>
                                    <small style="margin-left: 10px; margin-top: -10px">NB: Kosongkan jika tidak ingin mengubah password!</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Foto</label>
                                    <input type="file" class="form-control" name="foto">
                                    <small>NB: Kosongkan jika tidak ingin mengubah foto!</small>
                                </div>
                                <div class="form-group">
                                    <a href="#" data-toggle="modal" data-target="#viewFoto{{$member->id_member}}">
                                        <img src="{{asset('images/member/'.$member->foto)}}" alt="" class="img-fluid" height="100px" width="100px">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" placeholder="Masukan Username" name="username" value="{{$member->username}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" placeholder="Masukan Email" name="email" value="{{$member->email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <input type="text" class="form-control" placeholder="Masukan Kota" name="kota" value="{{$member->kota}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <input type="text" class="form-control" placeholder="Masukan Provinsi" name="provinsi" value="{{$member->provinsi}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No HP</label>
                                            <input type="text" class="form-control" placeholder="Masukan No HP" name="no_hp" value="{{$member->no_hp}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <input type="text" class="form-control" placeholder="Masukan Keterangan" name="keterangan" value="{{$member->keterangan}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status Member</label>
                                            <select name="status" class="form-control" value="{{$member->status}}">
                                                <option value="1" {{$member->status == 1 ? 'selected' : ''}}>Aktif</option>
                                                <option value="0" {{$member->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status Jastip</label>
                                            <select name="status_jastip" class="form-control" value="{{$member->status_jastip}}">
                                                <option value="diterima" {{$member->status_jastip == 'diterima' ? 'selected' : ''}}>Diterima</option>
                                                <option value="belum" {{$member->status_jastip == 'belum' ? 'selected' : ''}}>Belum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control" cols="30" rows="3">{{$member->alamat}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Update
                        </button>
                        <a href="{{url('admin/member')}}" class="btn btn-secondary">
                            <i class="fa fa-arrow-left"></i>
                            Kembali
                        </a>
                    </div>

                    <div class="modal fade" id="viewFoto{{$member->id_member}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h3 class="modal-title" id="exampleModalLabel">Foto Member : {{$member->nama}}</h3>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                              <div class="modal-body">
                                <img src="{{asset('images/member/'.$member->foto)}}" alt="" class="img-fluid">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                          </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });

        function showFoto(){
            $('#viewFoto').modal('show')
        }
    </script>
@endsection