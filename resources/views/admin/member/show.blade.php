@extends('admin.layouts.master',['activeMenu' => 'data-member'])
@section('title', 'Detail Member')
@section('breadcrumb', 'Detail Member')
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle"
                        src="{{asset('images/member/'.$member->foto)}}"
                        alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{$member->nama}}</h3>

                    <p class="text-muted text-center">{{$member->username}}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>NIK</b> <a class="float-right">{{$member->nik}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="float-right">{{$member->email}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>No HP</b> <a class="float-right">{{$member->no_hp}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><span class="nav-link active">Profile</span></li>
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <form class="form-horizontal">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Nama
                                    </div>
                                    <div class="form-group">
                                        Username
                                    </div>
                                    <div class="form-group">
                                        NIK
                                    </div>
                                    <div class="form-group">
                                        Email
                                    </div>
                                    <div class="form-group">
                                        Alamat
                                    </div>
                                    <div class="form-group">
                                        Kota
                                    </div>
                                    <div class="form-group">
                                        Provinsi
                                    </div>
                                    <div class="form-group">
                                        No HP
                                    </div>
                                    <div class="form-group">
                                        Keterangan
                                    </div>
                                    <div class="form-group">
                                        Status Member
                                    </div>
                                    <div class="form-group">
                                        Status Jastip
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        : {{$member->nama}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->username}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->nik}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->email}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->alamat}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->kota}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->provinsi}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->no_hp}}
                                    </div>
                                    <div class="form-group">
                                        : {{$member->keterangan}}
                                    </div>
                                    <div class="form-group">
                                        : 
                                        @if($member->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        : 
                                        @if($member->status == 'diterima')
                                            <span class="lb success">Diterima</span>
                                        @else
                                            <span class="lb warning">Belum</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{url('admin/member')}}" class="btn btn-default">Kembali</a>
                </div>
            </div>
        </div>
    </div>
@endsection