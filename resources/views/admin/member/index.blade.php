@extends('admin.layouts.master',['activeMenu' => 'data-member'])
@section('title', 'Data Member')
@section('breadcrumb', 'Data Member')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Member</h3>
                </div>
                <div class="card-body">
                    <table id="tableMember" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>No HP</th>
                                <th>Status</th>
                                <th>Status Jastip</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($members as $member)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$member->nama}}</td>
                                    <td>{{$member->username}}</td>
                                    <td>{{$member->email}}</td>
                                    <td>{{$member->no_hp}}</td>
                                    <td>
                                        @if($member->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($member->status_jastip == 'diterima')
                                            <span class="lb success">Diterima</span>
                                        @else
                                            <span class="lb warning">Belum</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{url('admin/member/show/'.$member->id_member)}}" class="fa fa-eye"></a>
                                        <a href="{{url('admin/member/'.$member->id_member.'/edit')}}" class="fa fa-pencil"></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#tableMember').DataTable();
        });
    </script>
@endsection