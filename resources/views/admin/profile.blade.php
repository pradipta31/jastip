@extends('admin.layouts.master',['activeMenu' => 'profile'])
@section('title', 'Profile Admin')
@section('breadcrumb', 'Profile Admin')
@section('content')
    <div class="row">
        <div class="col-md-3">

            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">

                        @if($admin->foto == null)
                            <img class="profile-user-img img-fluid img-circle"
                            src="{{asset('images/gallery/profile.jpeg')}}"
                            alt="User profile picture">
                        @else
                            <img class="profile-user-img img-fluid img-circle"
                            src="{{asset('images/admin/'.$admin->foto)}}"
                            alt="User profile picture">
                        @endif
                    </div>

                    <h3 class="profile-username text-center">{{$admin->nama}}</h3>

                    <p class="text-muted text-center">Admin</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Nama</b> <a class="float-right">{{$admin->nama}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="float-right">{{$admin->email}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Username</b> <a class="float-right">{{$admin->username}}</a>
                        </li>
                    </ul>

                    <button class="btn btn-primary btn-block" id="editProfile">
                        <i class="fa fa-user-edit"></i>
                        Edit Profile
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card" id="formProfile" style="display: none">
                <div class="card-body">
                    <form action="{{url('admin/profile/'.$admin->id_admin.'/edit')}}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="put">

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control" placeholder="Masukan nama" value="{{$admin->nama}}">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" name="username" class="form-control" placeholder="Masukan username" value="{{$admin->username}}">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" placeholder="Masukan email" value="{{$admin->email}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Masukan password">
                                <small>Kosongkan jika tidak ingin mengubah password</small>
                            </div>
                            
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Re-type Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="confirmation_password" id="confirmation_password" class="form-control" placeholder="Masukan password ulang">
                                <span id="message"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <textarea name="alamat" class="form-control" cols="30" rows="3">{{$admin->alamat}}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No Handphone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Masukan No HP" name="no_hp" value="{{$admin->no_hp}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Foto Profile</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="foto">
                                <small>NB: Kosongkan jika tidak ingin mengubah foto!</small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Keterangan</label>
                            <div class="col-sm-10">
                                <input type="text" name="keterangan" class="form-control" placeholder="Masukan keterangan" value="{{$admin->keterangan}}">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Simpan Perubahan
                                </button>
                            </div>
                        </div>
                    </form>
                        
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#editProfile').click( function() {
                $('#formProfile').toggle('slow');
            });
        });

        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });
    </script>
@endsection