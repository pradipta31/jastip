@extends('admin.layouts.master',['activeMenu' => 'rating'])
@section('title', 'Data Rating')
@section('breadcrumb', 'Data Rating')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Rating</h3>
                </div>
                <div class="card-body">
                    <table id="tableRating" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Rating</th>
                                <th>Komentar</th>
                                <th>Status</th>
                                {{-- <th>Opsi</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($ratings as $rating)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$rating->nama}}</td>
                                    <td>{{$rating->email}}</td>
                                    <td>
                                        @if($rating->rating == 1)
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                        @elseif($rating->rating == 2)
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                        @elseif($rating->rating == 3)
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                        @elseif($rating->rating == 4)
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star.png')}}" alt="" height="20px" width="20px">
                                        @elseif($rating->rating == 5)
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                            <img src="{{asset('images/star2.png')}}" alt="" height="20px" width="20px">
                                        @endif
                                    </td>
                                    <td>{{$rating->komentar}}</td>
                                    <td>
                                        @if($rating->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </td>
                                    {{-- <td>
                                        <a href="{{url('admin/member/show/'.$member->id_member)}}" class="fa fa-eye"></a>
                                        <a href="{{url('admin/member/'.$member->id_member.'/edit')}}" class="fa fa-pencil"></a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#tableRating').DataTable();
        });
    </script>
@endsection