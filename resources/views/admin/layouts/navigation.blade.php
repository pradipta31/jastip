<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        
        <img src="{{asset('images/gallery/logo.jpeg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8; width: 50px">
        <span class="brand-text font-weight-light">JASTIP</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @if(Session::get('id_admin'))
                    @if($admin->foto == null)
                        <img src="{{asset('images/gallery/profile.jpeg')}}" class="img-circle elevation-3" alt="User Image" style="width: 50px">
                    @else
                        <img src="{{asset('images/admin/'.$admin->foto)}}" class="img-circle elevation-3" alt="User Image" style="width: 50px">
                    @endif
                @elseif(Session::get('id_member'))
                    <img src="{{asset('images/member/'.$member->foto)}}" class="img-circle elevation-3" alt="User Image">
                @endif
            </div>
            <div class="info">
                @if(Session::get('id_admin'))
                    <a href="#" class="d-block">{{$admin->nama}}</a>
                    <span style="color: #c2c7d0">Admin</span>
                @elseif(Session::get('id_member'))
                    <a href="#" class="d-block">{{$member->nama}}</a>
                    <span style="color: #c2c7d0">Member</span>
                @endif
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                
                @if(Session::get('id_admin'))
                    <li class="nav-item">
                        <a href="{{url('admin')}}" class="nav-link {{$activeMenu == 'home' ? 'active' : ''}}">
                            <i class="fa fa-home"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview {{$activeMenu == 'tambah-member' ? 'menu-open' : ''}}{{$activeMenu == 'data-member' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Member
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('admin/member/tambah')}}" class="nav-link {{$activeMenu == 'tambah-member' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tambah Member</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/member')}}" class="nav-link {{$activeMenu == 'data-member' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Member</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview {{$activeMenu == 'tambah-jastip' ? 'menu-open' : ''}}{{$activeMenu == 'data-jastip' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-truck"></i>
                            <p>
                                Jasa Titip
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('admin/jastip/tambah')}}" class="nav-link {{$activeMenu == 'tambah-jastip' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tambah Jasa Titip</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/jastip')}}" class="nav-link {{$activeMenu == 'data-jastip' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Jasa Titip</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('admin/rating')}}" class="nav-link {{$activeMenu == 'rating' ? 'active' : ''}}">
                            <i class="fa fa-star"></i>
                            <p>
                                Rating
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('admin/profile')}}" class="nav-link {{$activeMenu == 'profile' ? 'active' : ''}}">
                            <i class="fa fa-user-edit"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>
                @elseif(Session::get('id_member'))
                    <li class="nav-item">
                        <a href="{{url('member/dashboard')}}" class="nav-link {{$activeMenu == 'dashboard' ? 'active' : ''}}">
                            <i class="fa fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('member/jastip')}}" class="nav-link {{$activeMenu == 'jastip' ? 'active' : ''}}">
                            <i class="fa fa-truck"></i>
                            <p>
                                Jasa Titip
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('member/rating')}}" class="nav-link {{$activeMenu == 'rating' ? 'active' : ''}}">
                            <i class="fa fa-star"></i>
                            <p>
                                Rating
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('member/profile')}}" class="nav-link {{$activeMenu == 'profile' ? 'active' : ''}}">
                            <i class="fa fa-user-edit"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>
                @endif

            </ul>
        </nav>
    </div>
</aside>