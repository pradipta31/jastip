<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Jastip - @yield('title')</title>
    @toastr_css
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/label.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @yield('css')
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        @include('admin.layouts.header')
        @include('admin.layouts.navigation', ['activeMenu' => $activeMenu])

        <div class="content-wrapper">
            @include('admin.layouts.breadcrumb')

            <div class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
        @include('admin.layouts.footer')
    </div>
    @jquery
    @toastr_js
    @toastr_render
    <script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
    @yield('js')
</body>
</html>
