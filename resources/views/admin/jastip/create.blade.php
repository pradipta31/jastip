@extends('admin.layouts.master',['activeMenu' => 'tambah-jastip'])
@section('title', 'Tambah Jastip')
@section('breadcrumb', 'Tambah Jastip')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Jasa Titip Baru</h3>
                </div>
                <form role="form" action="{{url('admin/jastip/tambah')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Member</label>
                                    <select name="member_id" class="select2" value="{{old('member_id')}}" style="line-height: 18px; width: 100%">
                                        @foreach($members as $member)
                                            <option value="{{$member->id_member}}">{{$member->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kota</label>
                                    <input type="text" class="form-control" placeholder="Masukan Kota" name="kota" value="{{old('kota')}}">
                                </div>
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <input type="text" class="form-control" placeholder="Masukan Provinsi" name="provinsi" value="{{old('provinsi')}}">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Awal</label>
                                    <input type="date" class="form-control" name="tanggal_awal" value="{{old('tanggal_awal')}}">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Akhir</label>
                                    <input type="date" class="form-control" name="tanggal_akhir" value="{{old('tanggal_akhir')}}">
                                </div>
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" class="form-control" name="foto">
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea name="keterangan" class="form-control" cols="30" rows="10">{{old('keterangan')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control" value="{{old('status')}}">
                                        <option value="1">Aktif</option>
                                        <option value="0">Non Aktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/select2/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('.select2').select2()
    </script>
@endsection