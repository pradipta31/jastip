@extends('admin.layouts.master',['activeMenu' => 'data-jastip'])
@section('title', 'Data Jasa Titip')
@section('breadcrumb', 'Data Jasa Titip')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Jastip</h3>
                </div>
                <div class="card-body">
                    <table id="tableJastip" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Member</th>
                                <th>Kota</th>
                                <th>Provinsi</th>
                                <th>Tanggal Awal</th>
                                <th>Tanggal Akhir</th>
                                <th width="25%">Keterangan</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($jastips as $jastip)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$jastip->member->nama}}</td>
                                    <td>{{$jastip->kota}}</td>
                                    <td>{{$jastip->provinsi}}</td>
                                    <td>{{date('d-m-Y', strtotime($jastip->tanggal_awal))}}</td>
                                    <td>{{date('d-m-Y', strtotime($jastip->tanggal_akhir))}}</td>
                                    <td>
                                        <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Keterangan
                                        </a>
                                        <div class="collapse" id="collapseExample">
                                            {{$jastip->keterangan}}
                                        </div>
                                    </td>
                                    <td>
                                        @if($jastip->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{url('admin/jastip/'.$jastip->id_jastip.'/edit')}}" class="fa fa-pencil"></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#tableJastip').DataTable();
        });
    </script>
@endsection