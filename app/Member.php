<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'members';
    protected $fillable = [
        'username', 'password', 'nama', 'alamat', 'kota', 'provinsi','email', 'no_hp', 
        'nik', 'foto', 'keterangan', 'status', 'admin_id', 'status_jastip',
    ];

    protected $primaryKey = 'id_member';

    public function jastip(){
        return $this->hasOne('App\Jastip');
    }
}
