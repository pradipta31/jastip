<?php

namespace App\Http\Controllers\Pengunjung;

use Session;
use Validator;
use App\Rating;
use App\Jastip;
use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
        $jastips = Jastip::where('status', 1)->get();
        return view('pengunjung.home.index', compact('jastips'));
    }

    public function jastipDetail($id){
        $jastip = Jastip::where('id_jastip',$id)->first();
        $ratings = Rating::where('member_id', $jastip->member_id)->get();
        return view('pengunjung.jastip.detail', compact('jastip', 'ratings'));
    }

    public function jastip(){
        $jastips = Jastip::where('status', 1)->get();
        return view('pengunjung.jastip.index', compact('jastips'));
    }

    public function postRating(Request $r){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'email' => 'required',
            'rating' => 'required',
            'komentar' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $rating = Rating::create([
                'member_id' => $r->member_id,
                'nama' => $r->nama,
                'email' => $r->email,
                'rating' => $r->rating,
                'komentar' => $r->komentar,
                'status' => 1
            ]);
            toastSuccess('Komentar anda berhasil dikirim!');
            return redirect()->back();
        }
    }

    public function contactUs(){
        return view('pengunjung.contact.index');
    }
}
