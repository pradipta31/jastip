<?php

namespace App\Http\Controllers\Member;

use Session;
use App\Member;
use App\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    public function index(){
        $ratings = Rating::where('member_id',Session::get('id_member'))->get();
        $member = Member::where('id_member',Session::get('id_member'))->first();
        return view('member.rating.index', compact('ratings', 'member'));
    }
}
