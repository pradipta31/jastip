<?php

namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\Hash;
use Session;
use Image;
use Validator;
use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function formLogin(){
        return view('member.login');
    }

    public function loginProccess(Request $r){
        $username = $r->username; //buat variable untuk menyimpan username
        $password = $r->password; //buat variable untuk menyimpan username
        $data = Member::where('username',$username)->first(); //select ke table admin didatabase apa
        if ($data){ //jika username tersebut ada
            if ($data->status == '1') { //jika status adminnya aktif
                if(Hash::check($password, $data->password)){
                    Session::put('id_member', $data->id_member);
                    Session::put('logged_in', TRUE);
                    return redirect(url('member/dashboard'));
                }else{
                    Session::flash('error', 'Login Gagal! Username atau Password Salah!');
                    return redirect()->back()->WithInput();
                }
            }else{
                Session::flash('error', 'Login Gagal! Admin Nonaktif!');
                return redirect()->back()->WithInput();
            }
        }else{
            Session::flash('error', 'Login Gagal! Username atau Passwprd Salah!');
            return redirect()->back()->WithInput();
        }
    }
    public function logOut(){
        Session::flush();
        return redirect('member')->with('alert', 'anda sudah login');
    }

    public function profile(){
        $member = Member::where('id_member', Session::get('id_member'))->first();
        return view('member.profile', compact('member'));
    }

    public function profileUpdate(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'username' => 'required|max:25',
            'nama' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'email' => 'required',
            'no_hp' => 'required|max:15',
            'nik' => 'required|max:25',
            'keterangan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('foto') && $r->password != null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/member/'.$filename));
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'foto' => $filename,
                    'keterangan' =>  $r->keterangan,
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }elseif ($r->hasFile('foto') && $r->password == null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/member/'.$filename));
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'foto' => $filename,
                    'keterangan' =>  $r->keterangan,
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }elseif (!$r->hasFile('foto') && $r->password != null) {
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'keterangan' =>  $r->keterangan
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }elseif (!$r->hasFile('foto') && $r->password == null) {
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'keterangan' =>  $r->keterangan
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }
        }
    }
}
