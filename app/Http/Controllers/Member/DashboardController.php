<?php

namespace App\Http\Controllers\Member;

use Session;
use App\Jastip;
use App\Rating;
use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        if (!Session::get('logged_in')) {
            return redirect('member');
        }else{
            $countPane = [
                'jastip' => Jastip::all()->count(),
                'rating' => Rating::all()->count(),
                'member' => Member::where('id_member', Session::get('id_member'))->first()
            ];
            // $member = Member::where('id_member', Session::get('id_member'))->first();
            return view('member.dashboard', $countPane);
        }
    }
}
