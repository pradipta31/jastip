<?php

namespace App\Http\Controllers\Member;

use Session;
use App\Member;
use App\Jastip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JastipController extends Controller
{
    public function index(){
        if (!Session::get('logged_in')) {
            return redirect('member');
        }else{
            $member = Member::where('id_member', Session::get('id_member'))->first();
            $jastips = Jastip::where('member_id', Session::get('id_member'))
                       ->get();
            return view('member.jastip.index', compact('member', 'jastips'));
        }
    }
}
