<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Session;
use Validator;
use Image;
use App\Admin;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function prossesLogin(Request $r){
        $username = $r->username; //buat variable untuk menyimpan username
        $password = $r->password; //buat variable untuk menyimpan username
        $data = Admin::where('username',$username)->first(); //select ke table admin didatabase apa
        if ($data){ //jika username tersebut ada
            if ($data->status == '1') { //jika status adminnya aktif
                if(Hash::check($password, $data->password)){
                    Session::put('id_admin', $data->id_admin);
                    Session::put('logged_in', TRUE);
                    return redirect(url('admin'));
                }else{
                    Session::flash('error', 'Login Gagal! Username atau Passwprd Salah!');
                    return redirect()->back()->WithInput();
                }
            }else{
                Session::flash('error', 'Login Gagal! Admin Nonaktif!');
                return redirect()->back()->WithInput();
            }
        }else{
            Session::flash('error', 'Login Gagal! Username atau Passwprd Salah!');
            return redirect()->back()->WithInput();
        }
    }
    public function logOut(){
        Session::flush();
        return redirect('login')->with('alert', 'anda sudah login');
    }

    public function profile(){
        $admin = Admin::where('id_admin', Session::get('id_admin'))->first();
        return view('admin.profile', compact('admin'));
    }

    public function profileUpdate(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'username' => 'required|max:25',
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'no_hp' => 'required|max:15',
            'keterangan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('foto') && $r->password != null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/admin/'.$filename));
                $admins = Admin::where('id_admin',$id)->update([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'foto' => $filename,
                    'keterangan' =>  $r->keterangan,
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }elseif ($r->hasFile('foto') && $r->password == null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/admin/'.$filename));
                $admins = Admin::where('id_admin',$id)->update([
                    'username' => $r->username,
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'foto' => $filename,
                    'keterangan' =>  $r->keterangan,
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }elseif (!$r->hasFile('foto') && $r->password != null) {
                $admins = Admin::where('id_admin',$id)->update([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'keterangan' =>  $r->keterangan
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }elseif (!$r->hasFile('foto') && $r->password == null) {
                $admins = Admin::where('id_admin',$id)->update([
                    'username' => $r->username,
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'keterangan' =>  $r->keterangan
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }
        }
    }
}
