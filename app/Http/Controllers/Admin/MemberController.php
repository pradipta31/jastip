<?php

namespace App\Http\Controllers\Admin;

use Session;
use Validator;
use Image;
use App\Admin;
use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    public function tambahMember(){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        return view('admin.member.create', compact('admin'));
    }

    public function simpanMember(Request $r){
        $validator = Validator::make($r->all(), [
            'username' => 'required|max:25',
            'password' => 'required',
            'nama' => 'required|max:50',
            'alamat' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'email' => 'required|max:50',
            'no_hp' => 'required|max:15',
            'nik' => 'required|max:25',
            'foto' => 'required|image|mimes:jpg,jpeg,png|max:5024',
            'keterangan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $query = Member::where('username', $r->username)->first();
            if ($query == null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/member/'.$filename));
                $members = Member::create([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'foto' => $filename,
                    'status' => 1,
                    'admin_id' => Session::get('id_admin'),
                    'keterangan' =>  $r->keterangan,
                    'status_jastip' => 'belum'
                ]);
                toastSuccess('Data berhasil disimpan!');
                return redirect()->back();
            }else{
                toastError('Username sudah ada!');
                return redirect()->back()->withInput();
            }
        }
    }

    public function index(){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        $members = Member::all();
        return view('admin.member.index', compact('admin', 'members'));
    }

    public function editMember($id){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        $member = Member::where('id_member',$id)->first();
        return view('admin.member.edit', compact('admin', 'member'));
    }

    public function showMember($id){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        $member = Member::where('id_member',$id)->first();
        return view('admin.member.show', compact('admin', 'member'));
    }

    public function updateMember(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'username' => 'required|max:25',
            'nama' => 'required|max:50',
            'alamat' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'email' => 'required|max:50',
            'no_hp' => 'required|max:15',
            'nik' => 'required|max:25',
            'keterangan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('foto') && $r->password != null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/member/'.$filename));
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'foto' => $filename,
                    'status' => $r->status,
                    'keterangan' =>  $r->keterangan,
                    'status_jastip' => $r->status_jastip
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back()->withInput();
            }elseif(!$r->hasFile('foto') && $r->password != null){
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'foto' => $filename,
                    'status' => $r->status,
                    'keterangan' =>  $r->keterangan,
                    'status_jastip' => $r->status_jastip
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back()->withInput();
            }elseif ($r->hasFile('foto') && $r->password == null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/member/'.$filename));
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'foto' => $filename,
                    'status' => $r->status,
                    'keterangan' =>  $r->keterangan,
                    'status_jastip' => $r->status_jastip
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back()->withInput();
            }elseif (!$r->hasFile('foto') && $r->password == null) {
                $members = Member::where('id_member',$id)->update([
                    'username' => $r->username,
                    'nama' => $r->nama,
                    'alamat' => $r->alamat,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'email' => $r->email,
                    'no_hp' => $r->no_hp,
                    'nik' => $r->nik,
                    'status' => $r->status,
                    'keterangan' =>  $r->keterangan,
                    'status_jastip' => $r->status_jastip
                ]);
                toastSuccess('Data berhasil diubah!');
                return redirect()->back()->withInput();
            }
        }
    }
}
