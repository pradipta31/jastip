<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Member;
use App\Jastip;
use App\Rating;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        if (!Session::get('logged_in')) {
            return redirect('login');
        }else{
            $countPane = [
                'member' => Member::all()->count(),
                'jastip' => Jastip::all()->count(),
                'rating' => Rating::all()->count(),
                'admin' => Admin::where('id_admin', Session::get('id_admin'))->first()
            ];
            return view('admin.dashboard', $countPane);
        }
    }
}
