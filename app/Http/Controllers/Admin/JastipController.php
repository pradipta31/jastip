<?php

namespace App\Http\Controllers\Admin;

use Session;
use Validator;
use Image;
use App\Admin;
use App\Member;
use App\Jastip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JastipController extends Controller
{
    public function tambahJastip(){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        $members = Member::where('status_jastip', 'belum')
                         ->where('status', 1)
                         ->get();
        return view('admin.jastip.create', compact('admin', 'members'));
    }

    public function simpanJastip(Request $r){
        $validator = Validator::make($r->all(), [
            'member_id' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
            'foto' => 'required|image|mimes:jpeg,jpg,png|max:5024',
            'keterangan' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $query = Jastip::where('member_id', 'LIKE', '%'. $r->member_id .'%')
                            ->where('kota', 'LIKE', '%'. $r->kota .'%')
                            ->first();
            if ($query == null) {
                $foto = $r->file('foto');
                $filename = time() . '.' . $foto->getClientOriginalExtension();
                Image::make($foto)->save(public_path('/images/jastip/'.$filename));
                $jastip = Jastip::create([
                    'member_id' => $r->member_id,
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'tanggal_awal' => $r->tanggal_awal,
                    'tanggal_akhir' => $r->tanggal_akhir,
                    'foto' => $filename,
                    'keterangan' => $r->keterangan,
                    'status' => $r->status
                ]);
                if ($r->status == 1) {
                    $member = Member::where('id_member',$r->member_id)->update([
                        'status_jastip' => 'diterima'
                    ]);
                }else{
                    $member = Member::where('id_member',$r->member_id)->update([
                        'status_jastip' => 'belum'
                    ]);
                }
                toastSuccess('Data berhasil disimpan!');
                return redirect()->back();
            }else{
                toastError('Member sudah ada dalam daftar jasa titip!');
                return redirect()->back()->withInput();
            }
        }
    }

    public function index(){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        $jastips = Jastip::all();
        return view('admin.jastip.index', compact('admin', 'jastips'));
    }

    public function editJastip($id){
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        $jastip = Jastip::where('id_jastip',$id)->first();
        $member = Member::where('id_member',$jastip->member_id)->first();
        return view('admin.jastip.edit', compact('admin', 'jastip', 'member'));
    }

    public function updateJastip(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'member_id' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
            'keterangan' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('foto')) {
                $v = Validator::make($r->all(), [
                    'foto' => 'required|image|mimes:jpeg,jpg,png|max:5024'
                ]);
                if ($v->fails()) {
                    toastError($validator->messages()->first());
                    return redirect()->back()->withInput();
                }else{
                    $foto = $r->file('foto');
                    $filename = time() . '.' . $foto->getClientOriginalExtension();
                    Image::make($foto)->save(public_path('/images/jastip/'.$filename));
                    $jastip = Jastip::where('id_jastip',$id)->update([
                        'kota' => $r->kota,
                        'provinsi' => $r->provinsi,
                        'tanggal_awal' => $r->tanggal_awal,
                        'tanggal_akhir' => $r->tanggal_akhir,
                        'foto' => $filename,
                        'keterangan' => $r->keterangan,
                        'status' => $r->status
                    ]);
                    if ($r->status == 1) {
                        $member = Member::where('id_member',$r->member_id)->update([
                            'status_jastip' => 'diterima'
                        ]);
                    }else{
                        $member = Member::where('id_member',$r->member_id)->update([
                            'status_jastip' => 'belum'
                        ]);
                    }
                    toastSuccess('Data berhasil diubah!');
                    return redirect()->back();
                }
            }else{
                $jastip = Jastip::where('id_jastip',$id)->update([
                    'kota' => $r->kota,
                    'provinsi' => $r->provinsi,
                    'tanggal_awal' => $r->tanggal_awal,
                    'tanggal_akhir' => $r->tanggal_akhir,
                    'keterangan' => $r->keterangan,
                    'status' => $r->status
                ]);
                if ($r->status == 1) {
                    $member = Member::where('id_member',$r->member_id)->update([
                        'status_jastip' => 'diterima'
                    ]);
                }else{
                    $member = Member::where('id_member',$r->member_id)->update([
                        'status_jastip' => 'belum'
                    ]);
                }
                toastSuccess('Data berhasil diubah!');
                return redirect()->back();
            }
        }
    }
}
