<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Admin;
use App\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    public function index(){
        $ratings = Rating::all();
        $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
        return view('admin.rating.index', compact('ratings', 'admin'));
    }
}
