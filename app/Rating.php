<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'ratings';
    protected $primaryKey = 'id_rating';
    protected $fillable = [
        'member_id', 'nama', 'email', 'rating', 'komentar', 'status'
    ];
}
