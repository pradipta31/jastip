<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jastip extends Model
{
    protected $table = 'jastips';
    protected $fillable = [
        'member_id', 'kota', 'provinsi', 'tanggal_awal', 'tanggal_akhir', 'foto', 'keterangan', 'status'
    ];

    protected $primaryKey = 'id_jastip';

    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }
}
