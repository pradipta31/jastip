<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admins';
    protected $filleable = [
        'id_member', 'username', 'password', 'nama', 'alamat', 'email', 'no_hp', 'foto', 'keterangan', 'status'
    ];
}
