<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Pengunjung'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('jastip', 'HomeController@jastip');
    Route::get('jastip/detail/{id}', 'HomeController@jastipDetail');
    Route::post('rating', 'HomeController@postRating');
    Route::get('contact/us', 'HomeController@contactUs');
});

Auth::routes();
Route::post('login_prosses','AuthController@prossesLogin');
Route::get('logout','AuthController@logOut');
Route::get('admin/profile', 'AuthController@profile');
Route::put('admin/profile/{id}/edit', 'AuthController@profileUpdate');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/','DashboardController@index');

    Route::get('member/tambah', 'MemberController@tambahMember');
    Route::post('member/tambah', 'MemberController@simpanMember');
    Route::get('member', 'MemberController@index');
    Route::get('member/{id}/edit', 'MemberController@editMember');
    Route::put('member/{id}/edit', 'MemberController@updateMember');
    Route::get('member/show/{id}', 'MemberController@showMember');

    Route::get('jastip', 'JastipController@index');
    Route::get('jastip/tambah', 'JastipController@tambahJastip');
    Route::post('jastip/tambah', 'JastipController@simpanJastip');
    Route::get('jastip/{id}/edit', 'JastipController@editJastip');
    Route::put('jastip/{id}/edit', 'JastipController@updateJastip');

    Route::get('rating', 'RatingController@index');
    
});

Route::group(['prefix' => 'member', 'namespace' => 'Member'], function(){
    Route::get('/', 'AuthController@formLogin');
    Route::post('login_member', 'AuthController@loginProccess');
    Route::get('logout', 'AuthController@logOut');

    Route::get('dashboard', 'DashboardController@index');
    Route::get('jastip', 'JastipController@index');

    Route::get('rating', 'RatingController@index');

    Route::get('profile', 'AuthController@profile');
    Route::put('profile/{id}/edit', 'AuthController@profileUpdate');
});
